Controls:

	For custom
		Left Mouse	-	Shoot the physics gun
		Right mouse - 	Aim the camera
		WASD 		-	Move the camera forwards/backwards/left/right
		Shift		- 	Move the camera down
		Space bar	-	Move the camera up
		1			- 	Spawn entirely too many spheres
		2			- 	Spawn entirely too many AABB's
		
	For PhysX
		Left Mouse	-	Shoot the physics gun
		Right mouse - 	Aim the camera
		WASD 		-	Move the camera forwards/backwards/left/right
		Shift		- 	Move the camera down
		Space bar	-	Move the camera up
		Arrow Keys 	- 	Move the Kinematic Controller