#pragma once
#include "PhysX1.h"
using namespace physx;
//UNTESTED
class Terrain
{
	PxRigidActor* m_pxActor;
	glm::mat4 m_transform;
	Scene m_model;

	PhysX1* p_engine;

public:
	Terrain() = delete;
	Terrain(PhysX1* engine) : p_engine(engine)
	{}

	void GenerateHeightMap();
};