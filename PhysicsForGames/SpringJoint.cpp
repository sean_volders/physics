#include "SpringJoint.h"
#include "Gizmos.h"

SpringJoint::SpringJoint(RigidBody * connection1, RigidBody * connection2, 
	float springCoefficient, float dampening)
{
	_connections[0] = connection1;
	_connections[1] = connection2;
	this->springCoefficient = springCoefficient;
	this->dampening = dampening;
	this->restLength = glm::length(_connections[0]->pos - _connections[1]->pos);
	_shapeID = JOINT;
}

void SpringJoint::update(glm::vec3 gravity, float timeStep)
{
	glm::vec3 vecBetween = _connections[0]->pos - _connections[1]->pos;
	glm::vec3 x = (glm::length(vecBetween) - restLength) * (vecBetween / glm::length(vecBetween));
	//todo: v should account for angular momentum (->angularVel * cross(connection point - center mass)
	//f = -kx -bv
	glm::vec3 force = (-springCoefficient * x) - (dampening * _connections[0]->vel);

	if (!_connections[0]->isStatic)
		_connections[0]->ApplyForce(force);
	
	vecBetween = _connections[1]->pos - _connections[0]->pos;
	x = (glm::length(vecBetween) - restLength) * (vecBetween / glm::length(vecBetween));
	//f = -kx -bv
	force = -springCoefficient * x - (dampening * _connections[1]->vel);

	if (!_connections[1]->isStatic)
		_connections[1]->ApplyForce(force);
}

void SpringJoint::makeGizmo()
{
	Gizmos::addLine(_connections[0]->pos, _connections[1]->pos, glm::vec4(0.5f, 0.5f, 0, 1));
}
