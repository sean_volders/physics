#pragma once
#include "PhysX1.h"

//parts that make it up
enum RagDollParts
{
	NO_PARENT = -1,
	LOWER_SPINE,
	LEFT_PELVIS,
	RIGHT_PELVIS,
	LEFT_UPPER_LEG,
	RIGHT_UPPER_LEG,
	LEFT_LOWER_LEG,
	RIGHT_LOWER_LEG,
	UPPER_SPINE,
	LEFT_CLAVICLE,
	RIGHT_CLAVICLE,
	NECK,
	HEAD,
	LEFT_UPPER_ARM,
	RIGHT_UPPER_ARM,
	LEFT_LOWER_ARM,
	RIGHT_LOWER_ARM
};

const PxVec3 X_AXIS = PxVec3(1, 0, 0);
const PxVec3 Y_AXIS = PxVec3(0, 1, 0);
const PxVec3 Z_AXIS = PxVec3(0, 0, 1);

struct RagdollNode
{
	PxQuat globalRotation; //rot of this link in model space
	PxVec3 scaledGlobalPos; //pos of the link centre in world space
	int parentNodeIdx; //index of the parent node
	float halfLength; //half length of the capsule for this node
	float radius; //radius of the capsule for this node
	float parentLinkPos; //relative pos of link centre in parent to this node. 
		//0 is center of the node, -1 is left end of the capsule, 1 is right end.
	float childLinkPos; //relative pos of link center  in child
	char* name; //name of link
	PxArticulationLink* linkPtr;
	//constructor
	RagdollNode(PxQuat globalRotation, int parentNodeIdx, float halfLength, float radius,
		float parentLinkPos, float childLinkPos, char* name) :
		globalRotation(globalRotation), parentNodeIdx(parentNodeIdx), halfLength(halfLength),
		radius(radius), parentLinkPos(parentLinkPos), childLinkPos(childLinkPos), name(name)
	{}
};

class Ragdoll
{
	std::vector<RagdollNode*> ragdollData;
	PhysX1* p_engine;

public:
	PxArticulation* articulation;

	Ragdoll(PhysX1* p_engine) : p_engine(p_engine) {}

	void InitTestRagdoll();
	void MakeRagdoll(PxTransform worldPos, float scaleFactor, PxMaterial* ragdollMaterial);
};