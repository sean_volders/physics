#pragma once
#include <glm\glm.hpp>
#include "PhysicsObject.h"
#include <vector>

namespace Collision
{
	bool SphereVsSphere(PhysicsObject*, PhysicsObject*);
	bool SphereVsPlane(PhysicsObject*, PhysicsObject*);
	bool SphereVsAABB(PhysicsObject*, PhysicsObject*);
	bool SphereVsJoint(PhysicsObject*, PhysicsObject*);
	bool PlaneVsSphere(PhysicsObject*, PhysicsObject*);
	bool PlaneVsPlane(PhysicsObject*, PhysicsObject*);
	bool PlaneVsAABB(PhysicsObject*, PhysicsObject*);
	bool PlaneVsJoint(PhysicsObject*, PhysicsObject*);
	bool AABBVsPlane(PhysicsObject*, PhysicsObject*);
	bool AABBVsSphere(PhysicsObject*, PhysicsObject*);
	bool AABBVsAABB(PhysicsObject*, PhysicsObject*);
	bool AABBVsJoint(PhysicsObject*, PhysicsObject*);
	bool JointVsSphere(PhysicsObject*, PhysicsObject*);
	bool JointVsPlane(PhysicsObject*, PhysicsObject*);
	bool JointVsAABB(PhysicsObject*, PhysicsObject*);
	bool JointVsJoint(PhysicsObject*, PhysicsObject*);
}

class DIYPhysicsScene
{
public:
	glm::vec3 gravity;
	float timeStep;
	std::vector<PhysicsObject*> actors;
	
	void AddActor(PhysicsObject*);
	void RemoveActor(PhysicsObject*);

	void Update();
	void Draw(); //call at the bottom of update

	void CheckForCollisions();

	//void DebugScene();
};