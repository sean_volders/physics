#include "Ragdoll.h"

void Ragdoll::InitTestRagdoll()
{
	ragdollData.push_back(new RagdollNode(
		PxQuat(PxPi / 2.0f, Z_AXIS), NO_PARENT, 1, 3, 1, 1, "lower spine"));

	ragdollData.push_back(new RagdollNode(
		PxQuat(PxPi, Z_AXIS), LOWER_SPINE, 1, 1, -1, 1, "left pelvis"));
	ragdollData.push_back(new RagdollNode(
		PxQuat(0, Z_AXIS), LOWER_SPINE, 1, 1, -1, 1, "right pelvis"));

	ragdollData.push_back(new RagdollNode(
		PxQuat(PxPi / 2.0f + 0.2f, Z_AXIS), LEFT_PELVIS, 5, 2, -1, 1, "L upper leg"));
	ragdollData.push_back(new RagdollNode(
		PxQuat(PxPi / 2.0f - 0.2f, Z_AXIS), RIGHT_PELVIS, 5, 2, -1, 1, "R upper leg"));

	ragdollData.push_back(new RagdollNode(
		PxQuat(PxPi / 2.0f + 0.2f, Z_AXIS), LEFT_UPPER_LEG, 5, 1.75f, -1, 1, "L lower leg"));
	ragdollData.push_back(new RagdollNode(
		PxQuat(PxPi / 2.0f - 0.2f, Z_AXIS), RIGHT_UPPER_LEG, 5, 1.75f, -1, 1, "R lower leg"));

	ragdollData.push_back(new RagdollNode(
		PxQuat(PxPi / 2.0f, Z_AXIS), LOWER_SPINE, 1, 3, 1, -1, "upper spine"));

	ragdollData.push_back(new RagdollNode(
		PxQuat(PxPi, Z_AXIS), UPPER_SPINE, 1, 1.5f, 1, 1, "left clavicle"));
	ragdollData.push_back(new RagdollNode(
		PxQuat(0, Z_AXIS), UPPER_SPINE, 1, 1.5f, 1, 1, "right clavicle"));

	ragdollData.push_back(new RagdollNode(
		PxQuat(PxPi / 2.0f, Z_AXIS), UPPER_SPINE, 1, 1, 1, -1, "neck"));
	ragdollData.push_back(new RagdollNode(
		PxQuat(PxPi / 2.0f, Z_AXIS), NECK, 1, 3, 1, -1, "HEAD"));

	ragdollData.push_back(new RagdollNode(
		PxQuat(PxPi - 0.3f, Z_AXIS), LEFT_CLAVICLE, 3, 1.5f, -1, 1, "left upper arm"));
	ragdollData.push_back(new RagdollNode(
		PxQuat(0.3f, Z_AXIS), RIGHT_CLAVICLE, 3, 1.5f, -1, 1, "right upper arm"));

	ragdollData.push_back(new RagdollNode(
		PxQuat(PxPi - 0.3f, Z_AXIS), LEFT_UPPER_ARM, 3, 1, -1, 1, "left lower arm"));
	ragdollData.push_back(new RagdollNode(
		PxQuat(0.3f, Z_AXIS), RIGHT_UPPER_ARM, 3, 1, -1, 1, "right lower arm"));
}

void Ragdoll::MakeRagdoll(PxTransform worldPos, float scaleFactor, PxMaterial* ragdollMaterial)
{
	PxArticulation *funcArticulation = p_engine->m_physics->createArticulation();

	for (auto currentNode = ragdollData.begin(); currentNode != ragdollData.end(); ++currentNode)
	{
		//setup
		RagdollNode* currentNodePtr = *currentNode;
		RagdollNode* parentNode = nullptr;

		float radius = currentNodePtr->radius * scaleFactor;
		float halfLength = currentNodePtr->halfLength * scaleFactor;
		float childHalfLength = radius + halfLength;
		float parentHalfLength = 0; //will be set later if there's a parent

		PxArticulationLink* parentLinkPtr = NULL;
		currentNodePtr->scaledGlobalPos = worldPos.p;

		//link geometry to the parent (if there's a parent)
		if (currentNodePtr->parentNodeIdx != -1)
		{
			parentNode = ragdollData[currentNodePtr->parentNodeIdx];
			parentLinkPtr = parentNode->linkPtr;
			parentHalfLength = (parentNode->radius + parentNode->halfLength) * scaleFactor;
			//work out the local pos of the node
			PxVec3 currentRelative = currentNodePtr->childLinkPos
				* currentNodePtr->globalRotation.rotate(PxVec3(childHalfLength, 0, 0));
			PxVec3 parentRelative = -currentNodePtr->parentLinkPos
				* parentNode->globalRotation.rotate(PxVec3(parentHalfLength, 0, 0));
			currentNodePtr->scaledGlobalPos = parentNode->scaledGlobalPos - (parentRelative + currentRelative);
		}
		//build transform for the link
		PxTransform linkTransform = PxTransform(currentNodePtr->scaledGlobalPos,
			currentNodePtr->globalRotation);
		//create the link in the articulation
		PxArticulationLink* link = funcArticulation->createLink(parentLinkPtr, linkTransform);
		//add the pointer to this link into the ragdoll data so we have it for later when we want to link to it
		currentNodePtr->linkPtr = link;
		float jointSpace = 0.1f; //gap between joints
		float capsuleHalfLength = (halfLength > jointSpace ? halfLength - jointSpace : 0) + 0.1f;
		PxCapsuleGeometry capsule(radius, capsuleHalfLength);
		link->createShape(capsule, *ragdollMaterial); //adds a capsule collider to the link
		PxRigidBodyExt::updateMassAndInertia(*link, 50.0f); //add some mass, should be part of the data?

		if (currentNodePtr->parentNodeIdx != -1)
		{
			//set up the joint
			PxArticulationJoint *joint = link->getInboundJoint();
			PxQuat frameRotation = parentNode->globalRotation.getConjugate()
				* currentNodePtr->globalRotation;
			//set the parent constraint frame
			PxTransform parentConstraintFrame = PxTransform(
				PxVec3(currentNodePtr->parentLinkPos * parentHalfLength, 0, 0), frameRotation);
			//set the child constraint frame
			PxTransform thisConstraintFrame = PxTransform(PxVec3(currentNodePtr->childLinkPos * childHalfLength, 0, 0));
			joint->setParentPose(parentConstraintFrame);
			joint->setChildPose(thisConstraintFrame);
			//set some constants
			joint->setStiffness(20);
			joint->setDamping(20);
			joint->setSwingLimit(0.4f, 0.4f);
			joint->setSwingLimitEnabled(true);
			joint->setTwistLimit(-0.1f, 0.1f);
			joint->setTwistLimitEnabled(true);
		}
	}
	this->articulation = funcArticulation;
}
