#ifndef _APPLICATION_H_
#define _APPLICATION_H_
#include "FlyCamera.h"

struct GLFWwindow;

class Application
{
protected:
	float m_currTime;
	float m_dt;
	float m_prevTime;

	FlyCamera* camera;

public:
	Application();
	virtual ~Application();

	virtual bool startup();
	virtual void shutdown();

	virtual bool update();
	virtual void draw();
	
    int window_width;
    int window_height;
	GLFWwindow* m_window;

	bool m_clearGizmos = false;

	void predraw();
	void postdraw();
};

#endif //_APPLICATION_H_