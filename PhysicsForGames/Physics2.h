#pragma once
#include "Application.h"
#include "DIYPhysicsScene.h"
#include "ProjectilePath.h"

class Physics2 : public Application
{
	DIYPhysicsScene* scene;
	ProjectilePath* path;

	bool drawnPath = false;

public:
	virtual bool startup();
	virtual void shutdown();

	virtual bool update();
	virtual void draw();
};