#include "Physics1.h"
#include "SphereClass.h"
#include "glm\ext.hpp"

#include "Gizmos.h"

#include "gl_core_4_4.h"
#include "GLFW\glfw3.h"


bool Physics1::startup()
{
	m_clearGizmos = true;
	bool ret =  Application::startup();

	physicsScene = new DIYPhysicsScene();
	physicsScene->gravity = glm::vec3(0, 10, 0);
	physicsScene->timeStep = 0.1f;

	//add four balls
	RigidSphere *newBall, *newBall2;
	RigidSphere* sphere = new RigidSphere(glm::vec3(0), glm::vec3(0), 1, 2, glm::vec4(1, 0, 0, 1));
	physicsScene->AddActor(sphere);
	RigidSphere* sphere2 = new RigidSphere(glm::vec3(0, -10, 0), glm::vec3(0), 1, 1, glm::vec4(1));
	sphere2->isStatic = true;
	physicsScene->AddActor(sphere2);

	return ret;
}

void Physics1::shutdown()
{
	Application::shutdown();
}
 
bool Physics1::update()
{
	bool retVal = Application::update();
	physicsScene->Update();

	if (glfwGetKey(m_window, GLFW_KEY_SPACE) == GLFW_PRESS)
	{
		dynamic_cast<RigidBody*>(physicsScene->actors[0])->
			ApplyForceToActor(dynamic_cast<RigidBody*>(physicsScene->actors[1]), 
				glm::vec3(-10, 0, 0));
	}

	return retVal;
}

void Physics1::draw()
{
	Application::draw();
	physicsScene->Draw();
	Application::postdraw();
}
