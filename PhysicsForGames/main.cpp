#include <iostream>

#include "CustomPhysics.h"
#include "PhysX1.h"

int main()
{
    PhysX1 app;

    if (app.startup() == false)
    {
        return -1;
    }

    while (app.update() == true)
    {
        app.draw();
    }

    app.shutdown();

    return 0;
}