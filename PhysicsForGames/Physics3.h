#pragma once
#include "Application.h"
#include "DIYPhysicsScene.h"

class Physics3 : public Application
{
	class DIYPhysicsScene* scene;

public:

	virtual bool startup();
	virtual void shutdown();

	virtual bool update();
	virtual void draw();
};