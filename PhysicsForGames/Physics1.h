#pragma once
#include "Application.h"
#include "DIYPhysicsScene.h"

class Physics1 : public Application
{
	DIYPhysicsScene* physicsScene;

public:
	virtual bool startup();
	virtual void shutdown();

	virtual bool update();
	virtual void draw();
};