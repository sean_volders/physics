#pragma once
#include "Camera.h"

struct GLFWwindow;

class FlyCamera : public Camera
{
	float speed, rotSpeed;
	bool viewButtonClicked;
	double cursorX, cursorY;
	double prevCursorX, prevCursorY;

public:
	void Update(float dt, GLFWwindow* window);
	void SetSpeed(float speed);
	void SetRotationSpeed(float rotSpeed);

protected:
	void CalculateRotation(double dt, double xOffset, double yOffset);
};