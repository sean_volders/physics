#include "Terrain.h"

//untested
void Terrain::GenerateHeightMap()
{
	PxBoxGeometry box = PxBoxGeometry(1, 1, 1);
	PxTransform transform(*(PxMat44*)(&m_transform[0]));
	m_pxActor = PxCreateStatic(*p_engine->m_physics, transform, box, *p_engine->m_physicsMaterial);

	//todo: get vert and index data
	int cols = 0, rows = 0;
	PxHeightFieldSample* samples = new PxHeightFieldSample[cols * rows]; //todo: put a 1d array with heigh map here

	PxHeightFieldDesc hfDesc;
	hfDesc.format = PxHeightFieldFormat::eS16_TM;
	hfDesc.nbColumns = cols;
	hfDesc.nbRows = rows;
	hfDesc.samples.data = samples;
	hfDesc.samples.stride = sizeof(PxHeightFieldSample);
	hfDesc.thickness = -100.f;

	const float _heightScalePx = 5, _scale = 5; //todo: this thing...?
	PxHeightField* aAheightField = p_engine->m_physics->createHeightField(hfDesc);
	PxHeightFieldGeometry hfGeom(aAheightField, PxMeshGeometryFlags(), _heightScalePx,
		_scale, _scale);
	PxTransform pose = PxTransform(PxVec3(0.f, 0.f, 0.f));
	PxShape* heightMap = m_pxActor->createShape((PxHeightFieldGeometry)hfGeom,
		*p_engine->m_physicsMaterial, pose);
	assert(heightMap);
}
