#include "ProjectilePath.h"
#include "Gizmos.h"

void ProjectilePath::DrawAtTime(float time)
{
	float x = initialPos.x + (muzzleSpeed * time * cos(theta));
	float y = initialPos.y + (muzzleSpeed * time * sin(theta)) - (0.5f * gravity * (time * time));

	Gizmos::addSphere(glm::vec3(x, y, 0), 1, 10, 10, glm::vec4(1, 1, 1, 1));
}
