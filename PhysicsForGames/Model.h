#pragma once
#include "PhysX1.h"

using namespace physx;

class Model
{
public:
	PxRigidActor* m_pxActor;
	glm::mat4 m_transform;
	Scene m_model;
	PhysX1* p_engine;

	Model() = delete;
	Model(PhysX1* engine) : p_engine(engine) 
	{}

	//void Update();
	//void Draw();

	void LoadModel(char* dir, char* filename);
	void GenerateConvex(float density, PxMaterial* physicsMaterial);
	void GenerateMeshCollider();
};