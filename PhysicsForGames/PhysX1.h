#pragma once
#include "Application.h"
#include "Render.h"
#include <glm\ext.hpp>

#include <physx\PxPhysicsAPI.h>
#include <physx\PxScene.h>
#include <physx\pvd\PxVisualDebugger.h>

#include "ParticleEmitter.h"
#include "ParticleFluidEmitter.h"

#include <iostream>

using namespace physx;

class Ragdoll;
class Model;
class KinematicController;

class myAllocator : public physx::PxAllocatorCallback
{
public:
	virtual ~myAllocator() {}
	virtual void* allocate(size_t size, const char* typeName, const char* fileName,  int line)
	{
		void* pointer = _aligned_malloc(size, 16);
		return pointer;
	}
	virtual void deallocate(void* ptr)
	{
		_aligned_free(ptr);
	}
};

class MyCollisionCallback : public physx::PxSimulationEventCallback
{
	virtual void onContact(const PxContactPairHeader& pairHeader,
		const PxContactPair* pairs, PxU32 nbPairs)
	{
		//don't do anything special
	};
	virtual void onTrigger(PxTriggerPair* pairs, PxU32 nbPairs)
	{
		for (PxU32 i = 0; i < nbPairs; ++i)
		{
			const PxTriggerPair* pair = pairs + i;
			PxActor* triggerActor = pair->triggerActor;
			PxActor* otherActor = pair->otherActor;

			//store a value so the engine knows to create a box when it's ready
			//can't store scene in userdata and create here
			//note: investigate why
			if (triggerActor->userData)
			{
				int* ud = static_cast<int*>(triggerActor->userData);
				*ud += 1;
			}
		}
	};
	virtual void onConstraintBreak(PxConstraintInfo*, PxU32) {};
	virtual void onWake(PxActor**, PxU32) {};
	virtual void onSleep(PxActor**, PxU32) {};
};


class PhysX1 : public Application
{
public:
	physx::PxFoundation* m_physicsFoundation;
	physx::PxPhysics* m_physics;
	physx::PxScene* m_physicsScene;

	physx::PxDefaultErrorCallback m_defaultErrorCallback;
	physx::PxDefaultAllocator m_defaultAllocator;
	physx::PxSimulationFilterShader m_defaultFilterShader;

	physx::PxMaterial* m_physicsMaterial;
	physx::PxMaterial* m_boxMaterial;
	physx::PxCooking* m_physicsCooker;
	physx::PxControllerManager* m_controllerManager;

	ParticleFluidEmitter* m_particleEmitter;

	Renderer* m_renderer;

	std::vector<Model*> models;
	std::vector<Ragdoll*> ragdolls;
	std::vector<PxActor*> m_actors;

	PxRigidStatic* m_trigger;

	KinematicController* player;
public:
	bool startup();
	void shutdown();
	void SetupPhysXIntro();

	physx::PxScene* CreateDefaultScene();
	void SetUpVisualDebugger();

	void UpdatePhysX(float dt);

	bool update();
	void draw();

	//collision shape heirarchies
	void setupCSHTutorial();
	void updateCSHTutorial();

	void setupConvexTutorial();
	void updateConvecTutorial();
	
	void setupRagdollTutorial();
	void updateRagdollTutorial();

	void setupKinematicTutorial();
	void updateKinematicTutorial();

	void setupTriggerTutorial();
	void updateTriggerTutorial();

	void setupFluidTutorial();
	void updateFluidTutorial();

	glm::mat4 m_tank_transform;
	Scene m_scene;
	
	void AddWidget(physx::PxShape* shape, physx::PxRigidActor* actor, vec4 geo_color);
	void renderGizmos(physx::PxScene* physics_scene);
};