#include "Physics3.h"

#include "PlaneClass.h"
#include "SphereClass.h"
#include "AABBClass.h"

#include "gl_core_4_4.h"
#include <GLFW\glfw3.h>

#include <random>

bool Physics3::startup()
{
	m_clearGizmos = true;
	bool ret = Application::startup();

	scene = new DIYPhysicsScene();
	scene->gravity = glm::vec3(0, 10, 0);
	scene->timeStep = .1f;

	scene->AddActor(new RigidAABB(glm::vec3(20, 20, 1), glm::vec3(5, 5, 5), 
		glm::vec3(-10, -5, 0), 3, glm::vec4(0, 1, 0, 1)));
	scene->AddActor(new RigidAABB(glm::vec3(-20, 20, 1), glm::vec3(5, 5, 5), 
		glm::vec3(10, -5, 0), 3, glm::vec4(0, 1, 0, 1)));
	scene->AddActor(new RigidAABB(glm::vec3(-20, 50, 1), glm::vec3(5, 5, 5),
		glm::vec3(3, -7.5, 0), 3, glm::vec4(0, 1, 0, 1)));

	scene->AddActor(new RigidPlane(glm::vec3(0, 1, 0), -50));
	scene->AddActor(new RigidPlane(glm::vec3(0, -1, 0), -50));
	scene->AddActor(new RigidPlane(glm::vec3(-1, 0, 0), -90));
	scene->AddActor(new RigidPlane(glm::vec3(1, 0, 0), -90));

	return ret;
}

void Physics3::shutdown()
{
	Application::shutdown();
}

bool Physics3::update()
{
	if (glfwGetKey(m_window, GLFW_KEY_SPACE) == GLFW_PRESS)
	{
		for (int i = 0; i < 2; ++i)
		{
			glm::vec3 addPos(rand() % 100 - 50, rand() % 100 - 50, 0);
			glm::vec3 force((rand() & 100) - 50, (rand() & 100) - 50, 0);
			scene->AddActor(new RigidSphere(addPos, force, 5, 3, glm::vec4(0, 0, 1, 0)));
			//scene->AddActor(new AABBClass(addPos, glm::vec2(5, 5), force, 5, glm::vec4(0, 1, 0, 1)));
		}
	}

	bool ret = Application::update();
	scene->Update();
	return ret;
}

void Physics3::draw()
{
	Application::draw();
	scene->Draw();
	Application::postdraw();
}