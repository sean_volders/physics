#include "Model.h"

void Model::LoadModel(char* dir, char* filename)
{
	m_model = LoadSceneFromOBJ(dir, filename);
}

void Model::GenerateConvex(float density, PxMaterial * physicsMaterial)
{
	PxBoxGeometry box = PxBoxGeometry(1, 1, 1);
	PxTransform transform(*(PxMat44*)(&m_transform[0]));

	m_pxActor = PxCreateDynamic(*p_engine->m_physics, transform, box, *p_engine->m_physicsMaterial, density);
	m_pxActor->userData = this;

	//find the number of verts in the model
	int numVerts = 0;
	for (int i = 0; i < m_model.mesh_count; ++i)
	{
		Mesh* mesh = &m_model.meshes[i];
		numVerts += mesh->vertex_count;
	}

	//reserve enough space for a vert buffer
	PxVec3 *verts = new PxVec3[numVerts];
	int vertIDX = 0;

	//copy verts from all sub meshes and transform them into the same space
	for (int i = 0; i < m_model.mesh_count; ++i)
	{
		Mesh* mesh = &m_model.meshes[i];
		int innerNumVerts = mesh->vertex_count;
		for (int vertCount = 0; vertCount < innerNumVerts; ++vertCount)
		{
			glm::vec4 temp = m_transform * glm::vec4(mesh->vertex_data[vertCount].pos, 0);
			verts[vertIDX++] = PxVec3(temp.x, temp.y, temp.z);
		}
	}

	PxConvexMeshDesc convexDesc;
	convexDesc.points.count = numVerts;
	convexDesc.points.stride = sizeof(PxVec3);
	convexDesc.points.data = verts;
	convexDesc.flags = PxConvexFlag::eCOMPUTE_CONVEX;
	convexDesc.vertexLimit = 128;
	PxDefaultMemoryOutputStream* buf = new PxDefaultMemoryOutputStream();
	assert(p_engine->m_physicsCooker->cookConvexMesh(convexDesc, *buf));
	PxU8* contents = buf->getData();
	PxU32 size = buf->getSize();
	PxDefaultMemoryInputData input(contents, size);
	PxConvexMesh* convexMesh = p_engine->m_physics->createConvexMesh(input);
	PxTransform pose = PxTransform(PxVec3(0.0f, 0.0f, 0.0f));
	PxShape* convexShape = m_pxActor->createShape(PxConvexMeshGeometry(convexMesh),
		*p_engine->m_physicsMaterial, pose);
	int numShapes = m_pxActor->getNbShapes();
	PxShape** shapes = (PxShape**)_aligned_malloc(sizeof(PxShape*)*numShapes, 16);

	m_pxActor->getShapes(shapes, numShapes);
	m_pxActor->detachShape(**shapes);
	delete(verts);
	p_engine->m_physicsScene->addActor(*m_pxActor);
}

void Model::GenerateMeshCollider()
{
	PxBoxGeometry box = PxBoxGeometry(1, 1, 1);
	PxTransform transform(*(PxMat44*)(&m_transform[0]));
	m_pxActor = PxCreateStatic(*p_engine->m_physics, transform, box, *p_engine->m_physicsMaterial);

	m_pxActor->userData = this;

	//find the number of verts in the model
	int numVerts = 0, numIndices = 0;
	for (int i = 0; i < m_model.mesh_count; ++i)
	{
		Mesh* mesh = &m_model.meshes[i];
		numVerts += mesh->vertex_count;

		for (int j = 0; j < mesh->sub_mesh_count; ++j)
		{
			SubMesh* subMesh = &mesh->sub_meshes[j];
			numIndices += subMesh->index_count;
		}
	}

	//reserve enough space for a vert buffer
	PxVec3 *verts = new PxVec3[numVerts];
	PxU32 *indices = new PxU32[numIndices];
	int vertIDX = 0, indexIDX = 0;

	//copy verts from all sub meshes and transform them into the same space
	for (int i = 0; i < m_model.mesh_count; ++i)
	{
		Mesh* mesh = &m_model.meshes[i];
		int innerNumVerts = mesh->vertex_count;
		for (int vertCount = 0; vertCount < innerNumVerts; ++vertCount)
		{
			glm::vec4 temp = m_transform * glm::vec4(mesh->vertex_data[vertCount].pos, 0);
			verts[vertIDX++] = PxVec3(temp.x, temp.y, temp.z);
		}
		for (int j = 0; j < mesh->sub_mesh_count; ++j)
		{
			SubMesh* subMesh = &mesh->sub_meshes[j];
			for (int indexCount = 0; indexCount < subMesh->index_count; ++indexCount)
			{
				indices[indexIDX++] = subMesh->index_data[indexCount];
			}
		}
	}

	PxTriangleMeshDesc meshDesc;
	meshDesc.points.count = numVerts;
	meshDesc.points.stride = sizeof(PxVec3);
	meshDesc.points.data = verts;
	
	int triCount = numIndices / 3;
	meshDesc.triangles.count = triCount;
	meshDesc.triangles.stride = 3 * sizeof(PxU32);
	meshDesc.triangles.data = indices;

	PxDefaultMemoryOutputStream* buf = new PxDefaultMemoryOutputStream();
	assert(p_engine->m_physicsCooker->cookTriangleMesh(meshDesc, *buf));
	PxU8* contents = buf->getData();
	PxU32 size = buf->getSize();
	PxDefaultMemoryInputData input(contents, size);
	PxTriangleMesh* triangleMesh = p_engine->m_physics->createTriangleMesh(input);
	PxTransform pose = PxTransform(PxVec3(0.f, 0, 0.f));
	PxShape* convexShape = m_pxActor->createShape(
		PxTriangleMeshGeometry(triangleMesh), *p_engine->m_physicsMaterial, pose);	

	int numShapes = m_pxActor->getNbShapes();
	PxShape** shapes = (PxShape**)_aligned_malloc(sizeof(PxShape*)*numShapes, 16);
	m_pxActor->getShapes(shapes, numShapes);
	m_pxActor->detachShape(**shapes);

	delete(verts);
	delete(indices);
	p_engine->m_physicsScene->addActor(*m_pxActor);
}
