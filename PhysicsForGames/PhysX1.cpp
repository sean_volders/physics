#include "PhysX1.h"
#include "Gizmos.h"
#include <GLFW\glfw3.h>
#include "Model.h"
#include "Ragdoll.h"
#include "KinematicController.h"
#include <iostream>

bool PhysX1::startup()
{
	m_clearGizmos = true;
	bool ret = Application::startup();

	m_defaultFilterShader = PxDefaultSimulationFilterShader;
	m_physicsFoundation = PxCreateFoundation(PX_PHYSICS_VERSION, m_defaultAllocator, m_defaultErrorCallback);

	m_physics = PxCreatePhysics(PX_PHYSICS_VERSION, *m_physicsFoundation, PxTolerancesScale());

	PxInitExtensions(*m_physics);
	m_physicsMaterial = m_physics->createMaterial(1, 1, 0);
	m_physicsCooker = PxCreateCooking(PX_PHYSICS_VERSION, *m_physicsFoundation,
		PxCookingParams(PxTolerancesScale()));

	m_physicsScene = CreateDefaultScene();

	m_renderer = new Renderer();

	//SetupPhysXIntro();
	//setupCSHTutorial();
	//setupConvexTutorial();
	setupRagdollTutorial();
	setupKinematicTutorial();
	setupTriggerTutorial();
	setupFluidTutorial();

	return ret;
}
void PhysX1::shutdown()
{

	m_physicsScene->release();
	m_physicsFoundation->release();
	//m_physics->release(); //assert triggered here is a bug in physx that was fixed in a later version
	Application::shutdown();
}

void PhysX1::SetupPhysXIntro()
{
	m_physicsScene = CreateDefaultScene();

	//add a plane
	PxTransform pose = PxTransform(PxVec3(0.0f, 0.0f, 0.0f), PxQuat(PxHalfPi, PxVec3(0.0f, 0.0f, 1.0f)));
	PxRigidStatic* plane = PxCreateStatic(*m_physics, pose, PxPlaneGeometry(), *m_physicsMaterial);
	m_physicsScene->addActor(*plane);

	//add a box
	float density = 10;
	PxBoxGeometry box(2, 2, 2);
	PxTransform transform(PxVec3(0, 5, 0));
	PxRigidDynamic* dynamicActor = PxCreateDynamic(*m_physics, transform, box,
		*m_physicsMaterial, density);
	m_physicsScene->addActor(*dynamicActor);
}

bool PhysX1::update()
{
	bool retVal = Application::update();
	UpdatePhysX(m_dt);
	return retVal;
}
void PhysX1::draw()
{
	Application::draw();
	renderGizmos(m_physicsScene);

	for (auto i = models.begin(); i != models.end(); ++i)
	{
		for (int j = 0; j < (*i)->m_model.mesh_count; ++j)
		{
			m_renderer->PushMesh(&(*i)->m_model.meshes[j], (*i)->m_transform);
		}
	}
	for (auto i = ragdolls.begin(); i != ragdolls.end(); ++i)
	{
		PxArticulation* articulation = (*i)->articulation;
		PxU32 nLinks = articulation->getNbLinks();
		PxArticulationLink** links = new PxArticulationLink*[nLinks];
		articulation->getLinks(links, nLinks);

		while (nLinks--)
		{
			PxArticulationLink* link = links[nLinks];
			PxU32 nShapes = link->getNbShapes();
			PxShape** shapes = new PxShape*[nShapes];
			link->getShapes(shapes, nShapes);
			while (nShapes--)
			{
				AddWidget(shapes[nShapes], link, vec4(1, 0, 0, 1));
			}
		}
		delete[] links;
	}

	m_renderer->RenderAndClear(camera->GetProjectionView());
	Application::postdraw();
}

void PhysX1::setupCSHTutorial()
{
	m_physicsScene = CreateDefaultScene();
	m_scene = LoadSceneFromOBJ("./data/tank/", "battle_tank.obj");

	//plane
	PxTransform pose = PxTransform(PxVec3(0.0f, 0.0f, 0.0f), PxQuat(PxHalfPi, PxVec3(0.0f, 0.0f, 1.0f)));
	PxRigidStatic* plane = PxCreateStatic(*m_physics, pose, PxPlaneGeometry(), *m_physicsMaterial);
	m_physicsScene->addActor(*plane);


	//hitbox for tank, and tank
	PxBoxGeometry box = PxBoxGeometry(1, 1, 2.2f);
	PxTransform* transform = new PxTransform(*(PxMat44*)(&m_tank_transform[0]));
	PxRigidDynamic* tankActor = PxCreateDynamic(*m_physics, *transform, box, *m_physicsMaterial, 10);

	int numberShapes = tankActor->getNbShapes();
	PxShape* shapes;
	tankActor->getShapes(&shapes, numberShapes);
	PxTransform relativePose = PxTransform(PxVec3(0, 1, -0.6f)); //center of the box
	shapes->setGeometry(box);
	shapes->setLocalPose(relativePose);

	//gun hitbox
	box = PxBoxGeometry(0.2f, 0.2f, 0.8f);
	PxTransform shapeTransform(PxVec3(0, 1, 1.75f));
	PxShape* shape = tankActor->createShape(box, *m_physicsMaterial);
	if (shape)
	{
		shape->setLocalPose(shapeTransform);
	}

	tankActor->userData = transform;
	m_physicsScene->addActor(*tankActor);
}
void PhysX1::updateCSHTutorial()
{
	PxActorTypeFlags flags = PxActorTypeFlag::eRIGID_DYNAMIC;
	int actorCount = m_physicsScene->getNbActors(flags);

	for (int i = 0; i < actorCount; ++i)
	{
		PxActor* actor;
		m_physicsScene->getActors(flags, &actor, 1, i);
		if (actor->userData)
		{
			PxRigidActor* rigidActor = (PxRigidActor*)actor;
			PxMat44 m = rigidActor->getGlobalPose();
			mat4 *transform = (mat4*)actor->userData;
			*transform = *(mat4*)&m;
			m_tank_transform = *transform;
		}
	}
}

void PhysX1::setupConvexTutorial()
{
	m_physicsScene = CreateDefaultScene();

	//add a plane
	PxTransform pose = PxTransform(PxVec3(0.0f, 0.0f, 0.0f), PxQuat(PxHalfPi, PxVec3(0.0f, 0.0f, 1.0f)));
	PxRigidStatic* plane = PxCreateStatic(*m_physics, pose, PxPlaneGeometry(), *m_physicsMaterial);
	m_physicsScene->addActor(*plane);

	Model* tank = new Model(this);
	tank->LoadModel("./data/tank/", "battle_tank.obj");
	//tank->GenerateConvex(10, m_physicsMaterial);
	tank->GenerateMeshCollider();

	models.push_back(tank);
}
void PhysX1::updateConvecTutorial()
{
	PxActorTypeFlags flags = PxActorTypeFlag::eRIGID_DYNAMIC;
	int actorCount = m_physicsScene->getNbActors(flags);

	for (int i = 0; i < actorCount; ++i)
	{
		PxActor* actor;
		m_physicsScene->getActors(flags, &actor, 1, i);
		if (actor->userData)
		{
			PxRigidActor* rigidActor = (PxRigidActor*)actor;
			PxMat44 m = rigidActor->getGlobalPose();
			mat4 *transform = &((Model*)actor->userData)->m_transform;
			*transform = *(mat4*)&m;
			m_tank_transform = *transform;
		}
	}
}

void PhysX1::setupRagdollTutorial()
{
	Ragdoll* r = new Ragdoll(this);
	r->InitTestRagdoll();
	r->MakeRagdoll(PxTransform(PxVec3(0, 10, 0)), .1f, m_physicsMaterial);
	m_physicsScene->addArticulation(*r->articulation);
	
	ragdolls.push_back(r);
}
void PhysX1::updateRagdollTutorial()
{

}

void PhysX1::setupKinematicTutorial()
{
	PxTransform pose = PxTransform(PxVec3(0.0f, 0.0f, 0.0f), PxQuat(PxHalfPi, PxVec3(0.0f, 0.0f, 1.0f)));
	PxRigidStatic* plane = PxCreateStatic(*m_physics, pose, PxPlaneGeometry(), *m_physicsMaterial);
	m_physicsScene->addActor(*plane);

	player = new KinematicController(this);
	player->GenerateCapsule(PxExtendedVec3(0, 1, 0));
}
void PhysX1::updateKinematicTutorial()
{
	player->Update(m_dt);
}

#pragma region trigger
struct FilterGroup
{
	enum Enum
	{
		ePLAYER = (1 << 0),
		ePLATFORM = (1 << 1),
		eGROUND = (1 << 2)
	};
};

void setShapeAsTrigger(PxRigidActor* actorIn)
{
	PxRigidStatic* staticActor = actorIn->is<PxRigidStatic>();
	assert(staticActor);

	const PxU32 numShapes = staticActor->getNbShapes();
	PxShape** shapes = (PxShape**)_aligned_malloc(sizeof(PxShape*)*numShapes, 16);
	staticActor->getShapes(shapes, numShapes);
	for (PxU32 i = 0; i < numShapes; ++i)
	{
		shapes[i]->setFlag(PxShapeFlag::eSIMULATION_SHAPE, false);
		shapes[i]->setFlag(PxShapeFlag::eTRIGGER_SHAPE, true);
	}
}

void setupFiltering(PxRigidActor* actor, PxU32 filterGroup, PxU32 filterMask)
{
	PxFilterData filterData;
	filterData.word0 = filterGroup; //word0 = own ID
	filterData.word1 = filterMask; //word1 = id mask to filter pairs that trigger a contact callback

	const PxU32 numShapes = actor->getNbShapes();
	PxShape** shapes = (PxShape**)_aligned_malloc(sizeof(PxShape*)*numShapes, 16);
	actor->getShapes(shapes, numShapes);

	for (PxU32 i = 0; i < numShapes; ++i)
	{
		PxShape* shape = shapes[i];
		shape->setSimulationFilterData(filterData);
	}
	_aligned_free(shapes);
}

void PhysX1::setupTriggerTutorial()
{
	PxSimulationEventCallback* myCollisionCallback = new MyCollisionCallback();
	m_physicsScene->setSimulationEventCallback(myCollisionCallback);

	PxTransform pose = PxTransform(PxVec3(10, 0.0f, -10), PxQuat(PxHalfPi, PxVec3(0.0f, 0.0f, 1.0f)));
	PxBoxGeometry triggerBox = PxBoxGeometry(1, 2, 2);
	m_trigger = PxCreateStatic(*m_physics, pose, triggerBox, *m_physicsMaterial);
	
	int* ud = new int(0);
	m_trigger->userData = ud;

	setShapeAsTrigger(m_trigger);
	m_physicsScene->addActor(*m_trigger);
}
void PhysX1::updateTriggerTutorial()
{

}

PxFilterFlags myFilterShader(PxFilterObjectAttributes attributes0, PxFilterData filterData0,
	PxFilterObjectAttributes attributes1, PxFilterData filterData1,
	PxPairFlags& pairFlags, const void* constantBlock, PxU32 constantBlockSize)
{
	//let triggers through
	if (PxFilterObjectIsTrigger(attributes0) ||
		PxFilterObjectIsTrigger(attributes1))
	{
		pairFlags = PxPairFlag::eTRIGGER_DEFAULT;
		return PxFilterFlag::eDEFAULT;
	}

	//generate contacts for all that were not filtered above
	pairFlags = PxPairFlag::eCONTACT_DEFAULT;
	//trigger the contact callback for pairs (A, B) where the filtermask of A contains the ID of B and vice versa
	if ((filterData0.word0 & filterData1.word1) &&
		(filterData1.word0 & filterData0.word1))
	{
		pairFlags |= PxPairFlag::eNOTIFY_TOUCH_FOUND | PxPairFlag::eNOTIFY_TOUCH_LOST;
	}
	return PxFilterFlag::eDEFAULT;
}


#pragma endregion

void PhysX1::setupFluidTutorial()
{
	PxVec3 offset = PxVec3(-10, 0, -10);
	PxTransform pose = PxTransform(PxVec3(0, 0, 0) + offset,
		PxQuat(PxHalfPi, PxVec3(0, 0, 1)));
	PxRigidStatic* plane = PxCreateStatic(*m_physics, pose, PxPlaneGeometry(), *m_physicsMaterial);

	const PxU32 numShapes = plane->getNbShapes();
	m_physicsScene->addActor(*plane);

	PxBoxGeometry side1(4.5f, 1, 0.5f);
	PxBoxGeometry side2(0.5f, 1, 4.5f);
	pose = PxTransform(PxVec3(0, 0.5f, 4.0f) + offset);
	PxRigidStatic* box = PxCreateStatic(*m_physics, pose, side1, *m_physicsMaterial);

	m_physicsScene->addActor(*box);
	m_actors.push_back(box);

	pose = PxTransform(PxVec3(0, 0.5f, -4) + offset);
	box = PxCreateStatic(*m_physics, pose, side1, *m_physicsMaterial);
	m_physicsScene->addActor(*box);
	m_actors.push_back(box);

	pose = PxTransform(PxVec3(5, 0.5f, 0) + offset);
	box = PxCreateStatic(*m_physics, pose, side2, *m_physicsMaterial);
	m_physicsScene->addActor(*box);
	m_actors.push_back(box);

	pose = PxTransform(PxVec3(-4, 0.5, 0) + offset);
	box = PxCreateStatic(*m_physics, pose, side2, *m_physicsMaterial);
	m_physicsScene->addActor(*box);
	m_actors.push_back(box);

	//particle system
	PxParticleFluid* pf;
	PxU32 maxParticles = 4000;
	bool perParticleRestOffset = false;
	pf = m_physics->createParticleFluid(maxParticles, perParticleRestOffset);

	pf->setRestParticleDistance(0.3f);
	pf->setDynamicFriction(0.1f);
	pf->setStaticFriction(0.1f);
	pf->setDamping(0.1f);
	pf->setParticleMass(.1);
	pf->setRestitution(0);
	pf->setParticleBaseFlag(PxParticleBaseFlag::eCOLLISION_TWOWAY, true);
	pf->setStiffness(100);

	if (pf)
	{
		m_physicsScene->addActor(*pf);
		m_particleEmitter = new ParticleFluidEmitter(maxParticles, 
			PxVec3(0, 10, 0) + offset, pf, .1);
		m_particleEmitter->setStartVelocityRange(-0.001f, -250.0f, -0.001f,
			0.001f, -250.0f, 0.001f);
	}
}

void PhysX1::updateFluidTutorial()
{
	m_particleEmitter->update(m_dt);
	m_particleEmitter->renderParticles();
}


PxScene* PhysX1::CreateDefaultScene()
{
	PxSceneDesc sceneDesc(m_physics->getTolerancesScale());
	sceneDesc.gravity = PxVec3(0, -9.807f, 0);
	//sceneDesc.filterShader = myFilterShader;
	sceneDesc.filterShader = m_defaultFilterShader;
	sceneDesc.cpuDispatcher = PxDefaultCpuDispatcherCreate(8);

	PxScene* res = m_physics->createScene(sceneDesc);
	return res;
}

void PhysX1::SetUpVisualDebugger()
{
	//check if PvdConnectionManager is available
	if (m_physics->getPvdConnectionManager() == NULL)
		return;

	const char* pvd_host_ip = "127.0.0.1";
	int port = 5425;
	unsigned int timeout = 100;
	physx::PxVisualDebuggerConnectionFlags connectionFlags
		= physx::PxVisualDebuggerExt::getAllConnectionFlags();
	auto connection = physx::PxVisualDebuggerExt::createConnection(
		m_physics->getPvdConnectionManager(), pvd_host_ip, port, timeout, connectionFlags);
}

void PhysX1::UpdatePhysX(float dt)
{
	if (dt <= 0)
		return;

	if (glfwGetMouseButton(m_window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS)
	{
		//transform
		vec3 camPos = camera->GetPosition();
		vec3 boxVel = camera->GetForward() * 20.f;
		PxTransform sphereTransform(PxVec3(camPos.x, camPos.y, camPos.z));

		//geometry
		PxSphereGeometry sphere(0.5f);
		float density = 10;

		//make it
		PxRigidDynamic* dynamicActor = PxCreateDynamic(*m_physics, sphereTransform, sphere,
			*m_physicsMaterial, density);

		const float muzzleSpeed = 50;
		//give it velocity
		glm::vec3 direction(camera->GetForward());
		PxVec3 velocity = PxVec3(direction.x, direction.y, direction.z) * muzzleSpeed;

		dynamicActor->setLinearVelocity(velocity, true);
		m_physicsScene->addActor(*dynamicActor);
	}

	//create boxes when you enter/exit trigger
	if (m_trigger->userData)
	{
		int* ud = static_cast<int*>(m_trigger->userData);
		int _ud = *ud;

		for (int i = 0; i < _ud; ++i)
		{
			PxTransform newBoxTransform(PxVec3(0, 10, 0));
			PxBoxGeometry box(0.25f, 0.25f, 0.25f);
			PxRigidDynamic* dynamicBox = PxCreateDynamic(*m_physics, newBoxTransform, box, *m_physicsMaterial, 10);
			m_physicsScene->addActor(*dynamicBox);
		}

		*ud = 0;
	}
	
	m_physicsScene->simulate(dt > 0.033f ? 0.033f : dt);
	while (m_physicsScene->fetchResults() == false)
	{

	}

	//updateCSHTutorial(); //
	//updateConvecTutorial(); //
	updateRagdollTutorial();
	updateKinematicTutorial();
	updateFluidTutorial();
}

void PhysX1::AddWidget(physx::PxShape* shape, physx::PxRigidActor* actor, vec4 geo_color)
{
	PxTransform full_transform = PxShapeExt::getGlobalPose(*shape, *actor);
	vec3 actor_position(full_transform.p.x, full_transform.p.y, full_transform.p.z);
	glm::quat actor_rotation(full_transform.q.w,
		full_transform.q.x,
		full_transform.q.y,
		full_transform.q.z);
	glm::mat4 rot(actor_rotation);

	mat4 rotate_matrix = glm::rotate(10.f, glm::vec3(7, 7, 7));

	PxGeometryType::Enum geo_type = shape->getGeometryType();

	switch (geo_type)
	{
	case (PxGeometryType::eBOX):
	{
		PxBoxGeometry geo;
		shape->getBoxGeometry(geo);
		vec3 extents(geo.halfExtents.x, geo.halfExtents.y, geo.halfExtents.z);
		PxShapeFlags flags = shape->getFlags();
		if (!(shape->getFlags() & PxShapeFlag::eSIMULATION_SHAPE))
		{
			Gizmos::addAABB(actor_position, extents, glm::vec4(1, 1, 1, 1), &rot);
		}
		else
			Gizmos::addAABB(actor_position, extents, geo_color, &rot);
	} break;
	case (PxGeometryType::eCAPSULE):
	{
		PxCapsuleGeometry geo;
		shape->getCapsuleGeometry(geo);
		Gizmos::addCapsule(actor_position, geo.halfHeight * 2, geo.radius, 16, 16, geo_color, &rot);
	} break;
	case (PxGeometryType::eSPHERE):
	{
		PxSphereGeometry geo;
		shape->getSphereGeometry(geo);
		Gizmos::addSphere(actor_position, geo.radius, 16, 16, geo_color, &rot);
	} break;
	case (PxGeometryType::ePLANE):
	{
		vec4 white(1);
		vec4 black(0, 0, 0, 1);
		for (int i = 0; i < 21; ++i)
		{
			Gizmos::addLine(vec3(-10 + i, 0, 10),
				vec3(-10 + i, 0, -10), i == 10 ? white : black);
			Gizmos::addLine(vec3(10, 0, -10 + i),
				vec3(-10, 0, -10 + i), i == 10 ? white : black);
		}
	} break;
	}
}

void PhysX1::renderGizmos(PxScene* physics_scene)
{
	PxActorTypeFlags desiredTypes = PxActorTypeFlag::eRIGID_STATIC | PxActorTypeFlag::eRIGID_DYNAMIC;
	PxU32 actor_count = physics_scene->getNbActors(desiredTypes);
	PxActor** actor_list = new PxActor*[actor_count];
	physics_scene->getActors(desiredTypes, actor_list, actor_count);

	vec4 geo_color(1, 0, 0, 1);
	for (int actor_index = 0;
		actor_index < (int)actor_count;
		++actor_index)
	{
		PxActor* curr_actor = actor_list[actor_index];
		if (curr_actor->isRigidActor())
		{
			PxRigidActor* rigid_actor = (PxRigidActor*)curr_actor;
			PxU32 shape_count = rigid_actor->getNbShapes();
			PxShape** shapes = new PxShape*[shape_count];
			rigid_actor->getShapes(shapes, shape_count);

			for (int shape_index = 0;
				shape_index < (int)shape_count;
				++shape_index)
			{
				PxShape* curr_shape = shapes[shape_index];
				AddWidget(curr_shape, rigid_actor, geo_color);
			}

			delete[]shapes;
		}
	}

	delete[] actor_list;

	int articulation_count = physics_scene->getNbArticulations();

	for (int a = 0; a < articulation_count; ++a)
	{
		PxArticulation* articulation;
		physics_scene->getArticulations(&articulation, 1, a);

		int link_count = articulation->getNbLinks();

		PxArticulationLink** links = new PxArticulationLink*[link_count];
		articulation->getLinks(links, link_count);

		for (int l = 0; l < link_count; ++l)
		{
			PxArticulationLink* link = links[l];
			int shape_count = link->getNbShapes();

			for (int s = 0; s < shape_count; ++s)
			{
				PxShape* shape;
				link->getShapes(&shape, 1, s);
				AddWidget(shape, link, geo_color);
			}
		}
		delete[] links;
	}
}