#pragma once
#include "PhysicsObject.h"
#include "glm\ext.hpp"

#define MIN_LINEAR_THRESHOLD 0.3f
#define MIN_ROTATION_THRESHOLD 0.01f

class RigidBody : public PhysicsObject
{
public:
	RigidBody(glm::vec3 pos, glm::vec3 vel, float rot, float mass, bool isStatic = false)
		:pos(pos), vel(vel), mass(mass), rot(rot), drag(0.98f), angularVelocity(0), elasticity(0.9f), angularDrag(0.98f),
		isStatic(isStatic)
	{}

	RigidBody(glm::vec3 pos, glm::vec3 vel, float rot, float mass,
		float drag, float angularVelocity, float elasticity, float angularDrag, bool isStatic = false)
		:pos(pos), vel(vel), mass(mass), rot(rot),
		drag(drag), angularVelocity(angularVelocity), elasticity(elasticity), angularDrag(angularDrag),
		isStatic(isStatic)
	{}

	float elasticity;

	float angularVelocity;
	glm::vec3 pos, vel;
	float mass;
	
	float rot;
	glm::mat4 rotMat;

	float drag, angularDrag;

	bool isStatic;

	virtual void update(glm::vec3 gravity, float timeStep);
	//virtual void debug();
	void ApplyForce(glm::vec3 force);
	void ApplyForceToActor(RigidBody* actor2, glm::vec3 force);
};