#include "FlyCamera.h"
#include "gl_core_4_4.h"
#include <glfw\glfw3.h>
#include <glm\gtx\rotate_vector.hpp>

void FlyCamera::Update(float dt, GLFWwindow* window)
{
	vec3 currPos = GetPosition();
	vec3 up = vec3(GetTransform() * vec4(0, 1, 0, 0));
	vec3 forward = vec3(GetTransform() * vec4(0, 0, -1, 0));
	vec3 right = vec3(GetTransform() * vec4(-1, 0, 0, 0));

	if (glfwGetKey(window, GLFW_KEY_W))
	{
		SetPosition(currPos + (forward * dt * speed));
	}
	if (glfwGetKey(window, GLFW_KEY_S))
	{
		SetPosition(currPos - (forward * dt * speed));
	}
	if (glfwGetKey(window, GLFW_KEY_A))
	{
		SetPosition(currPos + (right * dt * speed));
	}
	if (glfwGetKey(window, GLFW_KEY_D))
	{
		SetPosition(currPos - (right * dt * speed));
	}
	if (glfwGetKey(window, GLFW_KEY_SPACE))
	{
		SetPosition(currPos + (up * dt * speed));
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT))
	{
		SetPosition(currPos - (up * dt * speed));
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT))
	{
		
	}

	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_2) == GLFW_PRESS)
	{
		if (viewButtonClicked == false)
		{
			int width, height;
			glfwGetFramebufferSize(window, &width, &height);

			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
			glfwGetCursorPos(window, &prevCursorX, &prevCursorY);

			cursorX = width  * 0.5f;
			cursorY = height * 0.5f;

			glfwSetCursorPos(window, cursorX, cursorY);
			viewButtonClicked = true;
		}
		else
		{
			double mouseX;
			double mouseY;
			glfwGetCursorPos(window, &mouseX, &mouseY);

			double xOffset = mouseX - cursorX;
			double yOffset = mouseY - cursorY;

			CalculateRotation(dt, xOffset, yOffset);
		}
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);
		glfwSetCursorPos(window, width / 2, height / 2);
	}
	else
	{
		if (viewButtonClicked)
		{
			glfwSetCursorPos(window, prevCursorX, prevCursorY);
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		}
		viewButtonClicked = false;
	}
}

void FlyCamera::CalculateRotation(double dt, double xOffset, double yOffset)
{
	if (xOffset == 0 && yOffset == 0) return;

	if (xOffset != 0.0)
	{
		glm::mat4 rot = glm::rotate((float)(rotSpeed * dt * -xOffset), glm::vec3(0, 1, 0));
		SetTransform(GetTransform() * rot);
	}

	if (yOffset != 0.0)
	{
		glm::mat4 rot = glm::rotate((float)(rotSpeed * dt * -yOffset), glm::vec3(1, 0, 0));
		SetTransform(GetTransform() * rot);
	}

	//Clean up rotation
	glm::mat4 oldTrans = GetTransform();
	glm::mat4 trans;
	glm::vec3 worldUp = glm::vec3(0, 1, 0);

	//Right
	glm::vec3 oldForward = glm::vec3(oldTrans[2].x, oldTrans[2].y, oldTrans[2].z);
	trans[0] = glm::normalize(glm::vec4(glm::cross(worldUp, oldForward), 0));

	//Up
	glm::vec3 newRight = glm::vec3(trans[0].x, trans[0].y, trans[0].z);
	trans[1] = glm::normalize(glm::vec4(glm::cross(oldForward, newRight), 0));

	//Forward
	trans[2] = glm::normalize(oldTrans[2]);

	//Position
	trans[3] = oldTrans[3];
	SetTransform(trans);
}

void FlyCamera::SetSpeed(float speed)
{
	this->speed = speed;
}
void FlyCamera::SetRotationSpeed(float rotSpeed)
{
	this->rotSpeed = rotSpeed;
}