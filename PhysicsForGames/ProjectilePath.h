#pragma once
#include "glm\glm.hpp"

class ProjectilePath
{
	glm::vec2 initialPos;
	float theta, muzzleSpeed, gravity;

public:
	ProjectilePath(glm::vec2 initialPos, float theta, float muzzleSpeed, float gravity)
		: initialPos(initialPos), theta(theta), muzzleSpeed(muzzleSpeed), gravity(gravity)
	{}

	void DrawAtTime(float time);
};