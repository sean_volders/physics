#include "PlaneClass.h"
#include "Gizmos.h"

void RigidPlane::makeGizmo()
{
	float lineSegmentLength = 300;
	glm::vec3 centre = normal * distance;
	glm::vec3 parallel = glm::vec3(normal.y, -normal.x, normal.z); //rotated 90deg around z
	glm::vec4 colour(1, 0, 0, 1);

	glm::vec3 end = centre + (parallel * lineSegmentLength);
	glm::vec3 start = centre - (parallel * lineSegmentLength);
	Gizmos::addLine(start, end, colour);
}