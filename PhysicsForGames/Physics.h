#pragma once
#include "Application.h"
#include "FlyCamera.h"
#include "Render.h"

#include <physx/PxPhysicsAPI.h>
#include <physx/PxScene.h>

using namespace physx;
class Physics : public Application
{
public:
	virtual bool startup();
	virtual void shutdown();
    virtual bool update();
    virtual void draw();

	void renderGizmos(PxScene* physics_scene);

    Renderer* m_renderer;
    FlyCamera m_camera;
    float m_delta_time;
};
