#pragma once
#include "PhysX1.h"

class MyControllerHitReport : public PxUserControllerHitReport
{
public:
	virtual void onShapeHit(const PxControllerShapeHit &hit);
	virtual void onControllerHit(const PxControllersHit &hit) {};
	virtual void onObstacleHit(const PxControllerObstacleHit &hit) {};

	MyControllerHitReport() : PxUserControllerHitReport() {};

	PxVec3 getPlayerContactNormal() { return m_playerContactNormal; }
	void clearPlayerContactNormal() { m_playerContactNormal = PxVec3(0, 0, 0); }
	PxVec3 m_playerContactNormal;
};

class KinematicController
{
	PhysX1* p_engine;
	PxControllerManager* m_characterManager;
	PxController* m_playerController;

	MyControllerHitReport* myHitReport;

	float m_characterYVelocity;
	float m_characterRotation;
	float m_playerGravity;

public:
	KinematicController(PhysX1* p_engine)
		: p_engine(p_engine) {}
	
	void GenerateCapsule(PxExtendedVec3 startingPosition);
	void Update(float dt);
};