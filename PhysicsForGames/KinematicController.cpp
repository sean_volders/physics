#include "KinematicController.h"
#include <GLFW\glfw3.h>

void MyControllerHitReport::onShapeHit(const PxControllerShapeHit &hit)
{
	PxRigidActor* actor = hit.shape->getActor();
	m_playerContactNormal = hit.worldNormal;
	PxRigidDynamic* myActor = actor->is<PxRigidDynamic>();
	if (myActor)
	{
		//can apply forces to thigns we hit
	}
}

void KinematicController::GenerateCapsule(PxExtendedVec3 startingPosition)
{
	myHitReport = new MyControllerHitReport();
	m_characterManager = PxCreateControllerManager(*p_engine->m_physicsScene);

	//describe the controller
	PxCapsuleControllerDesc desc;
	desc.height = 1.6f;
	desc.radius = 0.4f;
	desc.position.set(0, 0, 0);
	desc.material = p_engine->m_physicsMaterial; //todo: another material for player
	desc.reportCallback = myHitReport;
	desc.density = 10;

	//create the player controller
	m_playerController = m_characterManager->createController(desc);
	m_playerController->setPosition(startingPosition);

	//set up some variables to controler player
	m_characterYVelocity = 0;
	m_characterRotation = 0;
	m_playerGravity = -0.5f;
	
	myHitReport->clearPlayerContactNormal();
	p_engine->m_physicsScene->addActor(*m_playerController->getActor());
}

void KinematicController::Update(float dt)
{
	bool onGround;
	float movementSpeed = 10.f;
	float rotSpeed = 1.0f;

	//if contact normal.y is >0.3 we assume solid ground
	//todo: this better
	if (myHitReport->getPlayerContactNormal().y > 0.3f)
	{
		m_characterYVelocity = -0.1f;
		onGround = true;
	}
	else
	{
		m_characterYVelocity += m_playerGravity * dt;
		onGround = false;
	}
	myHitReport->clearPlayerContactNormal();
	const PxVec3 up(0, 1, 0);
	PxVec3 velocity(0, m_characterYVelocity, 0);
	
	//scan the keys
	if (glfwGetKey(p_engine->m_window, GLFW_KEY_UP) == GLFW_PRESS)
	{
		velocity.z -= movementSpeed * dt;
	}
	if (glfwGetKey(p_engine->m_window, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		velocity.z += movementSpeed * dt;
	}
	if (glfwGetKey(p_engine->m_window, GLFW_KEY_LEFT) == GLFW_PRESS)
	{
		velocity.x -= movementSpeed * dt;
	}
	if (glfwGetKey(p_engine->m_window, GLFW_KEY_RIGHT) == GLFW_PRESS)
	{
		velocity.x += movementSpeed * dt;
	}
	//todo: jumping

	float minDistance = 0.001f;
	PxControllerFilters filter;

	PxQuat rot(m_characterRotation, PxVec3(0, 1, 0));
	//move the controller
	m_playerController->move(rot.rotate(velocity), minDistance, dt, filter);
}