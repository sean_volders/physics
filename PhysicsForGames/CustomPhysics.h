#pragma once
#include "Application.h"
#include "DIYPhysicsScene.h"

class CustomPhysics : public Application
{
	DIYPhysicsScene* physicsScene;
	float m_gunCooldown, m_currGunCooldown;

public: 
	virtual bool startup();
	virtual void shutdown();

	virtual bool update();
	virtual void draw();
};