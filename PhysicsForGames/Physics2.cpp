#include "Physics2.h"
#include "SphereClass.h"

bool Physics2::startup()
{
	bool ret = Application::startup();

	scene = new DIYPhysicsScene();
	scene->gravity = glm::vec3(0, 10, 0);
	scene->timeStep = 0.1f;

	RigidSphere* sphere = new RigidSphere(glm::vec3(0, 0, 0), glm::vec3(0), 0.70f, 2, glm::vec4(1, 0, 0, 1));
	sphere->ApplyForce(glm::vec3(10, 10, 0));
	scene->AddActor(sphere);

	path = new ProjectilePath(glm::vec2(0, 0), glm::pi<float>() * 0.25f, 20, 10);

	return ret;
}

void Physics2::shutdown()
{
	Application::shutdown();
}

bool Physics2::update()
{
	bool retVal = Application::update();
	scene->Update();
	return retVal;
}

void Physics2::draw()
{
	Application::draw();
	scene->Draw();

	if (!drawnPath)
	{
		for (float i = 0; i < 10; i += 0.1f)
		{
			path->DrawAtTime(i);
		}
		drawnPath = true;
	}

	Application::postdraw();
}
