#pragma once
#include <glm\vec3.hpp>
#include <vector>

class PhysicsObject;

class PhysicsEngine
{
public:
	glm::vec3 gravity;
	float timeStep;
	std::vector<PhysicsObject*> actors;

	void Register(PhysicsObject*);
	void Unregister(PhysicsObject*);

	void Update();
	void Draw();
	void CheckForCollisions();
};