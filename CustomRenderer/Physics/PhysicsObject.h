#pragma once
#include <glm\vec3.hpp>

//NUMSHAPES should always be equal to the last shapeType + 1
#ifndef NUMSHAPES
#define NUMSHAPES (JOINT+1)
#endif

enum ShapeType
{
	PLANE = 0,
	SPHERE = 1,
	BOX = 2,
	JOINT = 3,
};

class PhysicsObject
{
public:
	ShapeType shapeID;
	virtual void Update(glm::vec3 gravity, float timeStep) = 0;
	virtual void Draw() = 0;
};