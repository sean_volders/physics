#include "PhysicsEngine.h"
#include <glm\vec2.hpp>
#include <glm\glm.hpp>
#include <glm\ext.hpp>

#include "PhysicsObject.h"

#include "RigidBodies\RigidAABB.h"
#include "RigidBodies\RigidPlane.h"
#include "RigidBodies\RigidSphere.h"

void PhysicsEngine::Register(PhysicsObject* po)
{
	actors.push_back(po);
}

void PhysicsEngine::Unregister(PhysicsObject* po)
{
	auto i = actors.begin();
	for (; i != actors.end(); ++i)
	{
		if (*i == po)
			break;
	}

	if (i == actors.end())
		return;
	actors.erase(i);
}

void PhysicsEngine::Update()
{
	for (auto i = actors.begin(); i != actors.end(); ++i)
		(*i)->Update(gravity, timeStep);

	CheckForCollisions();
}

void PhysicsEngine::Draw()
{
	for (auto i = actors.begin(); i != actors.end(); ++i)
		(*i)->Draw();
}

//collision
namespace Collision
{
	bool SphereVsSphere(PhysicsObject*, PhysicsObject*);
	bool SphereVsPlane(PhysicsObject*, PhysicsObject*);
	bool SphereVsAABB(PhysicsObject*, PhysicsObject*);
	bool SphereVsJoint(PhysicsObject*, PhysicsObject*);
	bool PlaneVsSphere(PhysicsObject*, PhysicsObject*);
	bool PlaneVsPlane(PhysicsObject*, PhysicsObject*);
	bool PlaneVsAABB(PhysicsObject*, PhysicsObject*);
	bool PlaneVsJoint(PhysicsObject*, PhysicsObject*);
	bool AABBVsPlane(PhysicsObject*, PhysicsObject*);
	bool AABBVsSphere(PhysicsObject*, PhysicsObject*);
	bool AABBVsAABB(PhysicsObject*, PhysicsObject*);
	bool AABBVsJoint(PhysicsObject*, PhysicsObject*);
	bool JointVsSphere(PhysicsObject*, PhysicsObject*);
	bool JointVsPlane(PhysicsObject*, PhysicsObject*);
	bool JointVsAABB(PhysicsObject*, PhysicsObject*);
	bool JointVsJoint(PhysicsObject*, PhysicsObject*);
}

typedef bool(*fn)(PhysicsObject*, PhysicsObject*);
static fn collisionFunctionArray[] =
{
	// planes
	Collision::PlaneVsPlane, Collision::PlaneVsSphere, Collision::PlaneVsAABB, Collision::PlaneVsJoint,
	// spheres
	Collision::SphereVsPlane, Collision::SphereVsSphere, Collision::SphereVsAABB, Collision::SphereVsJoint,
	// boxes
	Collision::AABBVsPlane, Collision::AABBVsSphere, Collision::AABBVsAABB, Collision::AABBVsJoint,
	// joints
	Collision::JointVsPlane, Collision::JointVsSphere, Collision::JointVsAABB, Collision::JointVsJoint,
};

void PhysicsEngine::CheckForCollisions()
{
	int actorCount = actors.size();

	for (int outer = 0; outer < actorCount - 1; ++outer)
	{
		for (int inner = outer + 1; inner < actorCount; ++inner)
		{
			PhysicsObject* obj1 = actors[inner];
			PhysicsObject* obj2 = actors[outer];
			int _shapeID1 = obj1->shapeID;
			int _shapeID2 = obj2->shapeID;

			//using function pointers
			int functionIndex = ((_shapeID1 * NUMSHAPES) + _shapeID2);
			fn collisionFunctionPtr = collisionFunctionArray[functionIndex];
			if (collisionFunctionPtr != NULL)
			{
				collisionFunctionPtr(obj1, obj2);
			}
		}
	}
}

namespace Collision
{
	bool PlaneVsSphere(PhysicsObject* p, PhysicsObject* s)
	{
		return SphereVsPlane(s, p);
	}
	bool PlaneVsPlane(PhysicsObject* p1, PhysicsObject* p2)
	{
		return false;
	}
	bool PlaneVsAABB(PhysicsObject* p, PhysicsObject* a)
	{
		return AABBVsPlane(a, p);
	}
	bool PlaneVsJoint(PhysicsObject* p, PhysicsObject* k)
	{
		return false;
	}

	bool SphereVsSphere(PhysicsObject* s1, PhysicsObject* s2)
	{
		//cast the objects
		RigidSphere* sphere1 = dynamic_cast<RigidSphere*>(s1);
		RigidSphere* sphere2 = dynamic_cast<RigidSphere*>(s2);

		if (sphere1 != NULL && sphere2 != NULL)
		{
			glm::vec3 delta = sphere2->pos - sphere1->pos;
			float d = glm::length(delta);
			float intersection = sphere1->radius + sphere2->radius - d;

			if (intersection > 0)
			{
				if (sphere1->isStatic && sphere2->isStatic)
					return true;

				glm::vec3 collisionNormal = glm::normalize(delta);
				glm::vec3 separationVector = collisionNormal * intersection * .5f + (collisionNormal * 0.1f);
				glm::vec3 relativeVelocity = sphere1->vel - sphere2->vel;
				float dot = glm::dot(relativeVelocity, collisionNormal);
				glm::vec3 collisionVector = collisionNormal * (glm::dot(relativeVelocity, collisionNormal));
				float elasticity = sphere1->elasticity * sphere2->elasticity;
				glm::vec3 forceVector = collisionVector * (1.0f / (1 / sphere1->mass + 1 / sphere2->mass)) * elasticity;

				if (sphere1->isStatic)
				{
					sphere2->pos += separationVector * 2;
					sphere2->ApplyForce(forceVector);
					return true;
				}
				else if (sphere2->isStatic)
				{
					sphere1->pos -= separationVector * 2;
					sphere1->ApplyForce(-forceVector);
					return true;
				}

				//newtons third law
				sphere1->ApplyForceToActor(sphere2, forceVector);
				//seperate
				sphere1->pos -= separationVector;
				sphere2->pos += separationVector;
				return true;
			}
		}
		return false;
	}
	bool SphereVsPlane(PhysicsObject* s, PhysicsObject* p)
	{
		//cast
		RigidSphere *sphere = dynamic_cast<RigidSphere*>(s);
		RigidPlane *plane = dynamic_cast<RigidPlane*>(p);

		glm::vec3 collisionNormal = plane->normal;
		float sphereToPlane = glm::dot(sphere->pos, plane->normal) - plane->distance;
		float intersection = sphere->radius - sphereToPlane;
		if (intersection > 0)
		{
			if (sphere->isStatic)
				return true;

			//separation
			glm::vec3 separationVector = plane->normal * intersection;
			sphere->pos += separationVector;

			//find the collision point
			//the plane is always static so collision response only on sphere
			glm::vec3 planeNormal = plane->normal;
			if (sphereToPlane < 0)
			{
				planeNormal *= -1; //flip it
			}

			glm::vec3 forceVector = -1 * sphere->mass * planeNormal * (glm::dot(planeNormal, sphere->vel))
				* sphere->elasticity;

			sphere->vel = glm::vec3(0);
			sphere->ApplyForce(forceVector);
			return true;
		}
		return false;
	}
	bool SphereVsAABB(PhysicsObject* p1, PhysicsObject* p2)
	{
		return AABBVsSphere(p2, p1);
	}
	bool SphereVsJoint(PhysicsObject* s, PhysicsObject* j)
	{
		return false;
	}

	bool AABBVsPlane(PhysicsObject* a, PhysicsObject* p)
	{
		//from Real Time Collision, vol 1
		//by Christopher Ericson
		RigidAABB *aabb = dynamic_cast<RigidAABB*>(a);
		RigidPlane *plane = dynamic_cast<RigidPlane*>(p);

		//create projected radius
		glm::vec3 projectedRadius = (aabb->pos * plane->normal.x) + (aabb->pos * plane->normal.y) + (aabb->pos * plane->normal.z);

		//proceed much like SphereVsPlane
		float boxToPlane = glm::dot(plane->normal, aabb->pos) - plane->distance;
		float intersection = projectedRadius.length() - boxToPlane;

		if (intersection > 0)
		{
			if (aabb->isStatic)
				return true;

			glm::vec3 separationVector = plane->normal * intersection;
			aabb->pos += separationVector;

			glm::vec3 planeNorm = plane->normal;
			if (boxToPlane < 0)
				planeNorm *= -1;

			glm::vec3 forceVec = -1 * aabb->mass * planeNorm * (glm::dot(planeNorm, aabb->vel));
			float combinedElasticity = (aabb->elasticity + plane->elasticity) / 2.0f;
			aabb->ApplyForce(2 * forceVec * combinedElasticity);
			return true;
		}
		return false;
	}
	bool AABBVsSphere(PhysicsObject* a, PhysicsObject* s)
	{
		RigidAABB *AABB = dynamic_cast<RigidAABB*>(a);
		RigidSphere *Sphere = dynamic_cast<RigidSphere*>(s);

		float dmin = 0;
		glm::vec3 center = Sphere->pos;
		glm::vec3 min = glm::vec3(AABB->pos.x - (AABB->dim.x / 2), AABB->pos.y - (AABB->dim.y / 2), AABB->pos.z - (AABB->dim.z / 2));
		glm::vec3 max = glm::vec3(AABB->pos.x + (AABB->dim.x / 2), AABB->pos.y + (AABB->dim.y / 2), AABB->pos.z + (AABB->dim.z / 2));

		if (center.x < min.x)
		{
			dmin += pow(center.x - min.x, 2);
		}
		else if (center.x > max.x)
		{
			dmin += pow(center.x - max.x, 2);
		}

		if (center.y < min.y)
		{
			dmin += pow(center.y - min.y, 2);
		}
		else if (center.y > max.y)
		{
			dmin += pow(center.y - max.y, 2);
		}

		if (center.z < min.z)
		{
			dmin += pow(center.z - min.z, 2);
		}
		else if (center.z > max.z)
		{
			dmin += pow(center.z - max.z, 2);
		}

		if (dmin <= pow(Sphere->radius, 2))
		{
			if (AABB->isStatic && Sphere->isStatic)
				return true;

			//separation
			glm::vec3 deltaPos = Sphere->pos - AABB->pos;
			float distance = glm::length(deltaPos);
			glm::vec3 relativeVelocity = Sphere->vel - AABB->vel;
			glm::vec3 collisionNormal = glm::normalize(deltaPos);
			glm::vec3 collisionVector = collisionNormal * (glm::dot(relativeVelocity, collisionNormal));
			glm::vec3 forceVector = collisionVector * (1.0f / (1 / Sphere->mass + 1) / AABB->mass);

			forceVector.z = 0;

			if (AABB->isStatic)
			{
				Sphere->pos += collisionNormal * distance;
				Sphere->ApplyForce(forceVector * 2);
				return true;
			}
			else if (Sphere->isStatic)
			{
				AABB->pos -= collisionNormal * distance;
				AABB->ApplyForce(-forceVector * 2);
				return true;
			}

			//separate
			Sphere->pos += collisionNormal * distance * 0.5f;
			AABB->pos -= collisionNormal * distance * 0.5f;

			//reflect
			Sphere->ApplyForceToActor(AABB, forceVector);

			return true;
		}

		return false;
	}
	bool AABBVsAABB(PhysicsObject* p1, PhysicsObject* p2)
	{
		RigidAABB *AABB1 = dynamic_cast<RigidAABB*>(p1);
		RigidAABB *AABB2 = dynamic_cast<RigidAABB*>(p2);

		float xMin1 = AABB1->pos.x - (AABB1->dim.x * 0.5f);
		float xMax1 = AABB1->pos.x + (AABB1->dim.x * 0.5f);
		float xMin2 = AABB2->pos.x - (AABB2->dim.x * 0.5f);
		float xMax2 = AABB2->pos.x + (AABB2->dim.x * 0.5f);

		float yMin1 = AABB1->pos.y - (AABB1->dim.y * 0.5f);
		float yMax1 = AABB1->pos.y + (AABB1->dim.y * 0.5f);
		float yMin2 = AABB2->pos.y - (AABB2->dim.y * 0.5f);
		float yMax2 = AABB2->pos.y + (AABB2->dim.y * 0.5f);

		if (xMin1 < xMax2
			&& xMax1 > xMin2)
		{
			if (yMin1 < yMax2
				&& yMax1 > yMin2)
			{
				if (AABB1->isStatic && AABB2->isStatic)
					return true;

				//collision resolution
				glm::vec3 deltaPos = AABB1->pos - AABB2->pos;
				glm::vec3 relativeVelocity = AABB1->vel - AABB2->vel;
				glm::vec3 collisionNormal = glm::normalize(deltaPos);
				glm::vec3 collisionVector = collisionNormal * (glm::dot(relativeVelocity, collisionNormal));
				glm::vec3 forceVector = collisionVector * (1.0f / (1 / AABB1->mass + 1 / AABB1->mass));

				//separation
				float xExtent1 = (xMax1 - xMin1) / 2;
				float xExtent2 = (xMax2 - xMin2) / 2;
				float xOverlap = xExtent1 + xExtent2 - abs(deltaPos.x);

				float yExtent1 = (yMax1 - yMin1) / 2;
				float yExtent2 = (yMax2 - yMin2) / 2;
				float yOverlap = yExtent1 + yExtent2 - abs(deltaPos.y);;

				glm::vec3 separationNormal;
				float penetration;

				//todo:: z overlap
				if (xOverlap < yOverlap)
				{
					separationNormal = collisionNormal.x < 0 ? glm::vec3(1, 0, 0) : glm::vec3(-1, 0, 0);
					penetration = xOverlap;
				}
				else
				{
					separationNormal = collisionNormal.y < 0 ? glm::vec3(0, 1, 0) : glm::vec3(0, -1, 0);
					penetration = yOverlap;
				}

				if (AABB1->isStatic)
				{
					AABB2->ApplyForce(forceVector * 2);
					AABB2->pos += separationNormal * penetration;
					return true;
				}
				else if (AABB2->isStatic)
				{
					AABB1->ApplyForce(-forceVector * 2);
					AABB1->pos -= separationNormal * penetration;
					return true;
				}

				float combinedElasticity = (AABB1->elasticity + AABB2->elasticity) / 2.f;
				//newtons third law
				AABB1->ApplyForceToActor(AABB2, forceVector * combinedElasticity);

				//separate
				AABB1->pos -= separationNormal * penetration * 0.5f;
				AABB2->pos += separationNormal * penetration * 0.5f;

				return true;
			}
		}

		return false;
	}
	bool AABBVsJoint(PhysicsObject* a, PhysicsObject* j)
	{
		return false;
	}

	bool JointVsSphere(PhysicsObject* j, PhysicsObject* s) { return false; }
	bool JointVsPlane(PhysicsObject* j, PhysicsObject* p) { return false; }
	bool JointVsAABB(PhysicsObject* j, PhysicsObject* a) { return false; }
	bool JointVsJoint(PhysicsObject* j1, PhysicsObject* j2) { return false; }
}