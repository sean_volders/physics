#pragma once
#include "..\PhysicsObject.h"

class RigidBody;

class SpringJoint : public PhysicsObject
{
	RigidBody* _connections[2];
	float dampening;
	float restLength;
	float springCoefficient;

public:
	SpringJoint(RigidBody* connection1, RigidBody* connection2,
		float springCoefficient, float dampening);

	void Update(glm::vec3 gravity, float timeStep);
	//virtual void debug();
	void Draw();
};