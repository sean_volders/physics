#include "RigidPlane.h"
#include "..\..\Renderer\RenderEngine.h"

void RigidPlane::Draw()
{
	//float lineSegmentLength = 300;
	//glm::vec3 centre = normal * distance;
	//glm::vec3 parallel = glm::vec3(normal.y, -normal.x, normal.z); //rotated 90deg around z
	//glm::vec4 colour(1, 0, 0, 1);
	//glm::vec3 end = centre + (parallel * lineSegmentLength);
	//glm::vec3 start = centre - (parallel * lineSegmentLength);
	//Gizmos::addLine(start, end, colour);

	glm::mat4 transform = glm::mat4(1);
	//s
	//transform = glm::scale(transform, glm::vec3(10, 10, 10));
	//r
	transform *= glm::orientation(normal, glm::vec3(0, 1, 0));
	//t
	transform = glm::translate(transform, normal * distance);
	RE->AddPlane(transform);
}