#pragma once
#include "RigidBody.h"
class RigidPlane : public PhysicsObject
{
public:
	RigidPlane(glm::vec3 normal, float distance)
		: normal(normal), distance(distance), elasticity(0.95f)
	{
		shapeID = ShapeType::PLANE;
	}

	glm::vec3 normal;
	float distance;
	float elasticity;

	virtual void Update(glm::vec3 gravity, float timeStep) {};
	virtual void Draw();
};