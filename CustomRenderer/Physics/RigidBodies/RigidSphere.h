#pragma once

#include "RigidBody.h"
class RigidSphere : public RigidBody
{
private:

public:
	RigidSphere(glm::vec3 position,
		glm::vec3 velocity, float mass, float radius,
		glm::vec4 colour)
		: RigidBody::RigidBody(position, velocity, 0, mass),
		colour(colour), radius(radius)
	{
		shapeID = ShapeType::SPHERE;
	}

	float radius;
	glm::vec4 colour;

	virtual void Update(glm::vec3 gravity, float timeStep);
	virtual void Draw();
	//virtual void debug();
};