#include "RigidSphere.h"
#include "..\..\Renderer\RenderEngine.h"

void RigidSphere::Update(glm::vec3 gravity, float timeStep)
{
	RigidBody::Update(gravity, timeStep);
}

void RigidSphere::Draw()
{
	glm::mat4 transform = glm::mat4(1);
	transform = glm::scale(transform, glm::vec3(radius) * 2);
	//rotate
	transform = glm::translate(transform, pos);
	RE->AddSphere(transform);
	//Gizmos::addSphere(pos, radius, 10, 10, colour);
	//Gizmos::addSphere(glm::vec3(pos, 0), radius, 20, 20, colour);
}
