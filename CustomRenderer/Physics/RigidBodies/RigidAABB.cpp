#include "RigidAABB.h"
#include "..\..\Renderer\RenderEngine.h"

void RigidAABB::Update(glm::vec3 gravity, float timeStep)
{
	RigidBody::Update(gravity, timeStep);
}

void RigidAABB::Draw()
{
	glm::mat4 transform = glm::mat4(1);
	transform = glm::scale(transform, dim);
	//rotate
	transform = glm::translate(transform, pos);
	RE->AddCube(transform);
	//Gizmos::addAABBFilled(pos, dim * 0.5f, colour, &glm::mat4(1));
	//Gizmos::add2DAABBFilled(pos, dim * 0.5f, colour, &glm::mat4(1));
}