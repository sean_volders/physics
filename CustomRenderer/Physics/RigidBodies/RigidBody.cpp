#include "RigidBody.h"
#include "glm\ext.hpp"

void RigidBody::Update(glm::vec3 gravity, float timeStep)
{
	if (!isStatic)
	{
		pos += (vel * timeStep);
		vel *= drag;
		vel -= (gravity * timeStep);

		rotMat = glm::rotate(rot, glm::vec3(0, 0, 1));
		rot += angularVelocity * timeStep;

		if (glm::length(vel) < MIN_LINEAR_THRESHOLD)
		{
			vel = glm::vec3(0);
		}
		if (abs(angularVelocity) < MIN_ROTATION_THRESHOLD)
		{
			angularVelocity = 0;
		}
	}
}

void RigidBody::ApplyForce(glm::vec3 force)
{
	vel += (force / mass);
}

void RigidBody::ApplyForceToActor(RigidBody * actor2, glm::vec3 force)
{
	actor2->ApplyForce(force);
	ApplyForce(-force);
}