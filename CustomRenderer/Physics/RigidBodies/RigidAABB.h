#pragma once
#include "RigidBody.h"

class RigidAABB : public RigidBody
{
private:
	glm::vec4 colour;

public:
	RigidAABB(glm::vec3 position, glm::vec3 dimensions,
		glm::vec3 velocity, float mass, glm::vec4 colour)
		: RigidBody::RigidBody(position, velocity, 0, mass),
		colour(colour), dim(dimensions)
	{
		shapeID = ShapeType::BOX;
	}

	glm::vec3 dim;

	virtual void Update(glm::vec3 gravity, float timeStep);
	virtual void Draw();
};