#include "GameObject\GameObject.h"
#include "Renderer\RenderEngine.h"

#include "Physics\PhysicsEngine.h"
#include "Physics\RigidBodies\RigidSphere.h"
#include "Physics\RigidBodies\RigidAABB.h"
#include "Physics\RigidBodies\RigidPlane.h"
#include "Physics\Joints\SpringJoint.h"

bool m_running = true;
float m_currTime, m_dt, m_prevTime;

int main(int argc, char** argv)
{
	RE->Init(1366, 768);

	m_prevTime = (float)glfwGetTime();
	m_currTime = m_dt = 0;

	float m_physicsTimeStep = .1f;

	float m_physicsStepCount = 0;
	float m_physicsStep = 0.016f;

	PhysicsEngine physics;
	physics.gravity = glm::vec3(0, 10, 0);
	physics.timeStep = m_physicsTimeStep;

	RigidSphere* ball1;
	RigidSphere* ball2;
	float ballRadius = 1;
	float mass = 2;
	glm::vec4 ballColour = glm::vec4(1, 1, 1, 1);
	ball1 = new RigidSphere(glm::vec3(0, 40, 0), glm::vec3(0), mass, ballRadius, ballColour);
	ball1->drag = 0;
	ball1->elasticity = 0.9f;
	ball1->isStatic = true;
	physics.Register(ball1);

	int numBall = 10;
	for (int i = 1; i < numBall; ++i)
	{
		ball2 = new RigidSphere(glm::vec3(i*6.0f, 40, 0), glm::vec3(0), mass, ballRadius, ballColour);
		ball2->drag = 0;
		ball2->elasticity = 0.9f;
		physics.Register(ball2);
		SpringJoint* sj = new SpringJoint(ball1, ball2, 5, .3f);
		physics.Register(sj);
		ball1 = ball2;
	}

	ball1 = new RigidSphere(glm::vec3(0, 0, 0), glm::vec3(0), 90, 1, ballColour);
	ball1->isStatic = true;
	physics.Register(ball1);

	RigidPlane* plane = new RigidPlane(glm::vec3(0, 1, 0), -1);
	physics.Register(plane);

	RigidAABB *aabb = new RigidAABB(glm::vec3(0, 60, 0), glm::vec3(5), glm::vec3(0), 2, glm::vec4(0));
	aabb->elasticity = 0.25f;
	physics.Register(aabb);

	while (m_running)
	{
		glfwPollEvents();

		//close cleanly
		if (!(glfwWindowShouldClose(RE->m_window) == false &&
			glfwGetKey(RE->m_window, GLFW_KEY_ESCAPE) != GLFW_PRESS))
		{
			m_running = false;
		}

		m_currTime = (float)glfwGetTime();
		m_dt = m_currTime - m_prevTime;
		m_prevTime = m_currTime;

		m_physicsStepCount += m_dt;
		if (m_physicsStepCount >= m_physicsStep)
		{
			physics.Update();
			m_physicsStepCount = 0;
		}
		physics.Draw();
		RE->Update(m_dt);
		RE->Render();
	}
}