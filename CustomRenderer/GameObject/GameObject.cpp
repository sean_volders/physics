#include "GameObject.h"
#include "Component\Component.h"

GameObject::GameObject(glm::vec3 pos, std::string name, std::string tag)
	: name(name), tag(tag)
{
	transform = new glm::mat4(0);
	*transform = glm::translate(*transform, pos);
}

GameObject::~GameObject()
{
	components.clear();
}

void GameObject::Update(float dt)
{
	for (auto i = components.begin(); i != components.end(); ++i)
	{
		(*i)->Update(dt);
	}
}

void GameObject::AddComponent(Component* component)
{
	components.push_back(component);
}

//todo: some way of removing components