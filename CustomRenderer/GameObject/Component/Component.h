#pragma once
#include "..\GameObject.h"

class Component
{
public:
	GameObject* parent;
	glm::mat4 transform;

	Component(GameObject* parent, glm::mat4 transform = glm::mat4(1))
		: parent(parent), transform(transform) 
	{}
	~Component()
	{
		//don't delete parent
		parent = nullptr;
	}

	virtual void Update(float dt) = 0;
};