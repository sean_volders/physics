#pragma once
#include <glm\gtc\matrix_transform.hpp>
#include <string>
#include <vector>

class Component;

class GameObject
{
protected:
	glm::mat4* transform;
	std::string name, tag;

	std::vector<Component*> components;

public:
	GameObject(glm::vec3 pos, std::string name, std::string tag = "");
	~GameObject();

	void Update(float dt);
	void AddComponent(Component* component);
};