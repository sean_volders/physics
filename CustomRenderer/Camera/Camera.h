#pragma once
#include <glm\glm.hpp>

using glm::vec3;
using glm::vec4;
using glm::mat4;

class Camera
{
	mat4 worldTransform;
	mat4 projectionTransform;
	mat4 projectionViewTransform;
public:
	void SetPerspective(float fov, float aspectRatio, float near, float far);
	void LookAt(vec3 lookAt, vec3 up);
	vec3 GetPosition();
	void SetPosition(vec3 pos);
	void UpdateProjectionViewTransform();

	Camera::Camera();

	void SetTransform(mat4 transform);
	mat4 GetTransform();
	mat4 GetView();
	mat4 GetProjection();
	mat4 GetProjectionView();
};