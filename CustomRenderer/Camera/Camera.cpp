#include "Camera.h"
#include <glm\gtx\transform.hpp>
#include <math.h>

void Camera::SetPerspective(float fov, float aspectRatio, float near, float far)
{
	/*float s = 1 / (near * tan(fov / 2));

	float zz = -(far + near) / (float)(far - near);
	float wz = -(2 * far * near) / (float)(far - near);

	projectionTransform = mat4(aspectRatio * s, 0, 0, 0,
								0, s, 0, 0,
								0, 0, zz, -1,
								0, 0, wz, 0);
	UpdateProjectionViewTransform();*/
	projectionTransform = glm::perspective(fov, aspectRatio, near, far);
	UpdateProjectionViewTransform();
}

void Camera::LookAt(vec3 lookAt, vec3 up) 
{
	worldTransform = glm::inverse(glm::lookAt(GetPosition(), lookAt, up));
	UpdateProjectionViewTransform();
}

vec3 Camera::GetPosition()
{
	return vec3(worldTransform[3]);
}

void Camera::SetPosition(vec3 pos) 
{
	worldTransform[3] = vec4(pos, 1);
	UpdateProjectionViewTransform();
}

void Camera::UpdateProjectionViewTransform()
{
	projectionViewTransform = projectionTransform * glm::inverse(worldTransform);
}

Camera::Camera()
{	
	worldTransform = mat4(1);
	projectionTransform = mat4(1, 0, 0, 0, 
								0, 1, 0, 0,
								0, 0, 1, -1,
								0, 0, 0, 0);

	UpdateProjectionViewTransform();
}

void Camera::SetTransform(mat4 transform)
{
	worldTransform = transform;
	UpdateProjectionViewTransform();
}

mat4 Camera::GetTransform()
{
	return worldTransform;
}

mat4 Camera::GetView()
{
	return glm::inverse(worldTransform);
}

mat4 Camera::GetProjection()
{
	return projectionTransform;
}

mat4 Camera::GetProjectionView()
{
	return projectionViewTransform;
}