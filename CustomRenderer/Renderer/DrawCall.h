#pragma once
#include <glm\mat4x4.hpp>
#include <glm\vec3.hpp>

enum E_CALLTYPE
{
	VAO,
	LINE
};

class DrawCall
{
public:
	E_CALLTYPE m_drawType;

	DrawCall() = delete;
	virtual ~DrawCall() {}

	DrawCall(E_CALLTYPE callType) : m_drawType(callType)
	{}
};

class VAODrawCall : public DrawCall
{
public:
	unsigned int m_vao, m_numIndices;
	glm::mat4 m_transform;

	VAODrawCall(unsigned int vao, unsigned int numIndices, glm::mat4& transform)
		:  DrawCall(E_CALLTYPE::VAO), m_vao(vao), m_numIndices(numIndices), m_transform(transform)
	{}
	virtual ~VAODrawCall()
	{
	}
};

class LineDrawCall : public DrawCall
{
public:
	glm::vec3 m_min, m_max;
	glm::vec4 m_colour;

	LineDrawCall(glm::vec3 min, glm::vec3 max, glm::vec4 colour)
		: DrawCall(E_CALLTYPE::LINE), m_min(min), m_max(max), m_colour(colour)
	{}
	virtual ~LineDrawCall() {}
};