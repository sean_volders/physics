#include "RenderEngine.h"
//debug macros
#ifdef _DEBUG
#define TestShader(x, y) __TestShader(x, y)
#define TestProgram(x) __TestProgram(x)
#else
#define TestShader(x, y) void
#define TestProgram(x) void
#endif

void __TestShader(unsigned int shader, GLenum type);
void __TestProgram(unsigned int program);

void RenderEngine::Init(unsigned int screenW, unsigned int screenH)
{
	initted = true;
	running = true;
	m_screenW = screenW;
	m_screenH = screenH;

	InitGLFW();
	InitGL();
	InitPrimatives();
	InitFirstPassFrameBuffer();
	InitPostProcessing();

	LoadShader("Default");
	LoadShader("PostProcess");
}

void RenderEngine::InitGLFW()
{
	if (glfwInit() == false)
	{
		assert("GLFW failed to initialise");
		return;
	}

	m_window = glfwCreateWindow(m_screenW, m_screenH, "Physics", NULL, NULL);
	glfwMakeContextCurrent(m_window);
}

void RenderEngine::InitGL()
{
	if (ogl_LoadFunctions() == ogl_LOAD_FAILED)
	{
		assert("gl_core LoadFunctions failed");
	}

	glEnable(GL_DEPTH_TEST);
	glClearColor(0.25f, 0.25f, 0.25f, 1);

	//set up camera
	float fov = glm::pi<float>() * 0.25f;
	float aspect = 16 / 9.f;
	float n = 0.1f;
	float f = 1000.f;
	
	m_camera = new FlyCamera();
	m_camera->SetSpeed(100);
	m_camera->SetRotationSpeed(1);
	m_camera->SetPosition(vec3(10, 1, 10));
	m_camera->LookAt(vec3(0), vec3(0, -5, 0));
	m_camera->SetPerspective(fov, aspect, n, f);
}

void RenderEngine::InitPrimatives()
{
	primativeSphere = new RCSphere(30, 30);
	primativeCube = new RCCube();
	primativePlane = new RCPlane(5, 5);
}

//todo: re-create when resolution changes
void RenderEngine::InitFirstPassFrameBuffer()
{
	glGenFramebuffers(1, &m_firstPassFrameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, m_firstPassFrameBuffer);

	glGenTextures(1, &m_firstPassTexture);
	glBindTexture(GL_TEXTURE_2D, m_firstPassTexture);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, m_screenW, m_screenH);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_firstPassTexture, 0);

	glGenRenderbuffers(1, &m_firstPassRenderBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, m_firstPassRenderBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, m_screenW, m_screenH);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_firstPassRenderBuffer);

	GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, drawBuffers);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void RenderEngine::InitPostProcessing()
{
	glm::vec2 halfTexel = 1.0f / glm::vec2(m_screenW, m_screenH) * 0.5f;

	float vertexData[] = {
		-1, -1, 0, 1, halfTexel.x, halfTexel.y,
		1, 1, 0, 1, 1 - halfTexel.x, 1 - halfTexel.y,
		-1, 1, 0, 1, halfTexel.x, 1 - halfTexel.y,

		-1, -1, 0, 1, halfTexel.x, halfTexel.y,
		1, -1, 0, 1, 1 - halfTexel.x, halfTexel.y,
		1, 1, 0, 1, 1 - halfTexel.x, 1 - halfTexel.y,
	};

	glGenVertexArrays(1, &m_screenVAO);
	glBindVertexArray(m_screenVAO);

	glGenBuffers(1, &m_screenVBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_screenVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6 * 6, vertexData, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0); //pos
	glEnableVertexAttribArray(1); //texcoord

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 6, 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 6, ((char*)0) + 16);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

unsigned int RenderEngine::LoadShader(std::string name)
{
	//if it's already in the map return it
	auto i = shaders.find(name);
	if (i != shaders.end())
		return i->second;
	else if (name.compare("Default") == 0)
	{
		std::string vsSource = R"(#version 410
layout(location = 0) in vec4 Position; 
layout(location = 1) in vec4 Colour; 
uniform mat4 projectionView; 
uniform mat4 globalTransform;
out vec4 vColour; 

void main() 
{ 
	vColour = Colour; 
	gl_Position = projectionView * globalTransform * Position; 
})";

		std::string fsSource = R"(#version 410
in vec4 vColour; 
out vec4 FragColor; 
void main() 
{ 
	FragColor = vColour; 
})";

		return LoadShaderFromString(name, vsSource, fsSource);
	}
	else if (name.compare("PostProcess") == 0)
	{
		std::string vsSource = R"(#version 410
layout (location = 0) in vec4 position;
layout (location = 1) in vec2 texCoord;
out vec2 TexCoord;
void main()
{
	gl_Position = position;
	TexCoord = texCoord;
})";
		std::string fsSource = R"(#version 410
in vec2 TexCoord;
out vec4 FragColor;
uniform sampler2D framebufferTexture;
void main()
{
	FragColor = texture(framebufferTexture, TexCoord);
})";
		return LoadShaderFromString(name, vsSource, fsSource);
	}
	else
	{
		//do file loading
	}

	return -1; //this should never happen
}

/*takes a shader name, the vertex and fragment strings.
returns a handle to the shader program*/
unsigned int RenderEngine::LoadShaderFromString(std::string name, std::string vert, std::string frag)
{
	GLuint vS = glCreateShader(GL_VERTEX_SHADER);
	GLuint fS = glCreateShader(GL_FRAGMENT_SHADER);
	GLuint program = glCreateProgram();

	const char* c_str = vert.c_str();
	glShaderSource(vS, 1, &c_str, 0);
	glCompileShader(vS);
	glAttachShader(program, vS);

	TestShader(vS, GL_VERTEX_SHADER);

	c_str = frag.c_str();
	glShaderSource(fS, 1, &c_str, 0);
	glCompileShader(fS);
	glAttachShader(program, fS);

	TestShader(fS, GL_FRAGMENT_SHADER);

	glLinkProgram(program);
	TestProgram(program);

	glDeleteShader(vS);
	glDeleteShader(fS);

	shaders.insert(std::make_pair(name, program));
	return program;
}

void RenderEngine::Update(float dt)
{
	m_camera->Update(dt, m_window);
}

void RenderEngine::Render()
{
	//draw the scene
	FirstRenderPass();
	
	//post processing block
	unsigned int postProcessShader = shaders.at("PostProcess");
	glUseProgram(postProcessShader);
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0); //unbind framebuffer
	glViewport(0, 0, m_screenW, m_screenH);
	glClear(GL_DEPTH_BUFFER_BIT);
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_firstPassTexture);

	int loc = glGetUniformLocation(postProcessShader, "framebufferTexture");
	glUniform1i(loc, 0);
	
	glBindVertexArray(m_screenVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);

	glfwSwapBuffers(m_window);

	for (int i = 0; i < drawCalls.size(); ++i)
		delete drawCalls[i];
	drawCalls.clear();
}

void RenderEngine::FirstRenderPass()
{
	//todo: sort operations to limit shader switching (if used)
	//draw the scene as normal into a texture
	glBindFramebuffer(GL_FRAMEBUFFER, m_firstPassFrameBuffer);
	glViewport(0, 0, m_screenW, m_screenH); //reset viewport
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	unsigned int defaultShader = shaders.at("Default");
	glUseProgram(defaultShader);
	for (auto i = drawCalls.begin(); i < drawCalls.end(); ++i)
	{
		switch ((*i)->m_drawType)
		{
		case (E_CALLTYPE::VAO):
			{
				VAODrawCall* call = dynamic_cast<VAODrawCall*>(*i);
				int loc = glGetUniformLocation(defaultShader, "projectionView");
				glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(m_camera->GetProjectionView()));
				loc = glGetUniformLocation(defaultShader, "globalTransform");
				glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(call->m_transform));
				glBindVertexArray(call->m_vao);
				glLineWidth(1);
				glDrawElements(GL_TRIANGLES, call->m_numIndices, GL_UNSIGNED_INT, 0);
				break;
			} 
		case (E_CALLTYPE::LINE):
			{
				LineDrawCall* call = dynamic_cast<LineDrawCall*>(*i);
				int loc = glGetUniformLocation(defaultShader, "projectionView");
				glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(m_camera->GetProjectionView()));
				loc = glGetUniformLocation(defaultShader, "globalTransform");
				glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(glm::mat4(1)));
				
				GLuint vao, vbo;
				glGenVertexArrays(1, &vao);
				glBindVertexArray(vao);
				glGenBuffers(1, &vbo);
				glBindBuffer(GL_ARRAY_BUFFER, vbo);

				Vertex data[] = {
					{ call->m_min.x, call->m_min.y, call->m_min.z, 1,
					call->m_colour.r, call->m_colour.g, call->m_colour.b, call->m_colour.a },
					{ call->m_max.x, call->m_max.y, call->m_max.z, 1,
					call->m_colour.r, call->m_colour.g, call->m_colour.b, call->m_colour.a }
				};

				glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(Vertex), data, GL_STATIC_DRAW);
				glEnableVertexAttribArray(0); //pos
				glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 8, 0);
				glEnableVertexAttribArray(1); //colour
				glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 8, ((char*)0) + (sizeof(float) * 4));

				glLineWidth(3);
				glDrawArrays(GL_LINE, 0, 1);

				//clean up vao
				glBindVertexArray(0);
				glDeleteBuffers(1, &vao);
				glDeleteVertexArrays(1, &vbo);
				break;
			}
		default:	
			printf("Not a valid draw call");
			break;
		}
	}
}

void RenderEngine::AddSphere(glm::mat4& transform)
{
	drawCalls.push_back(
		new VAODrawCall(primativeSphere->m_vao, primativeSphere->m_numIndices, transform)
	); 
}

void RenderEngine::AddCube(glm::mat4& transform)
{
	drawCalls.push_back(
		new VAODrawCall(primativeCube->m_vao, primativeCube->m_numIndices, transform)
	);
}

void RenderEngine::AddPlane(glm::mat4& transform)
{
	drawCalls.push_back(
		new VAODrawCall(primativePlane->m_vao, primativePlane->m_numIndices, transform)
	);
}

void RenderEngine::AddLine(glm::vec3 min, glm::vec3 max)
{
	drawCalls.push_back(
		new LineDrawCall(min, max, glm::vec4(COLOR_YELLOW))
	);
}

//should be called with the macro defined above "TestShader"
void __TestShader(unsigned int shader, GLenum type)
{
	GLint success;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

	if (!success)
	{
		printf("Shader Error, type = ");
		if (type == GL_VERTEX_SHADER)
			printf("Vertex\n");
		else if (type == GL_FRAGMENT_SHADER)
			printf("Fragment\n");
		else if (type == GL_GEOMETRY_SHADER)
			printf("Geometry\n");

		int infoLogLength = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);
		char* infoLog = new char[infoLogLength];
		glGetShaderInfoLog(shader, infoLogLength, 0, infoLog);
		printf("%s\n", infoLog);
		delete[] infoLog;
	}
}

//should be called with the macro defined above "TestProgram"
void __TestProgram(unsigned int program)
{
	int success;
	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if (success == GL_FALSE)
	{
		int infoLogLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);
		char* infoLog = new char[infoLogLength];

		glGetProgramInfoLog(program, infoLogLength, 0, infoLog);
		printf("Error: failed to link shader program! \n");
		printf("%s\n", infoLog);
		delete[] infoLog;
	}
}