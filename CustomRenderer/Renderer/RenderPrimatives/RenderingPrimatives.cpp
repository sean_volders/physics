#include "RenderingPrimatives.h"

#include <glm\ext.hpp>
#include "..\RenderEngine.h"
#include <vector>
#include <math.h>

RCSphere::RCSphere(unsigned int rings, unsigned int sectors)
{
	float radius = 1;
	m_shader = RE->LoadShader("Default");

	const float R = 1.f / (float)(rings - 1);
	const float S = 1.f / (float)(sectors - 1);
	unsigned int r, s;

	std::vector<Vertex> verts;
	std::vector<GLuint> indices;

	verts.resize(rings * sectors);
	auto v = verts.begin();
	for (r = 0; r < rings; ++r) {
		for (s = 0; s < sectors; ++s)
		{
			
			const float x = cos(2 * glm::pi<float>() * s * S) * sin(glm::pi<float>() * r * R);
			const float y = sin(glm::half_pi<float>() + glm::pi<float>() * r * R);
			const float z = sin(2 * glm::pi<float>() * s * S) * sin(glm::pi<float>() * r * R);

			//todo:
			//texCoord x = s * S;
			//texCoord y = r * R;

			//todo:
			//normal.x = x;
			//normal.y = y;
			//normal.z = z;

			//position
			(*v).x = x * radius;
			(*v).y = y * radius;
			(*v).z = z * radius;
			(*v).w = 1;

			//todo: make this better
			//colour
			(*v).r = 1;
			(*v).g = 0;
			(*v).b = 0;
			(*v).a = 1;

			v++;
		}
	}

	indices.resize(rings * sectors * 6); // * 3 * 3
	auto i = indices.begin();
	for (r = 0; r < rings; ++r) {
		for (s = 0; s < sectors; ++s)
		{
			//todo: turn this into two triangles
			*i++ = r * sectors + s;					//top left
			*i++ = r * sectors + (s + 1);			//top right
			*i++ = (r + 1) * sectors + (s + 1);		//bottom right

			*i++ = r * sectors + s;					//top left 
			*i++ = (r + 1) * sectors + (s + 1);		//bottom right 
			*i++ = (r + 1) * sectors + s;			//bottom left
		}
	}

	m_numIndices = indices.size();

	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	glGenBuffers(1, &m_vbo);
	glGenBuffers(1, &m_ibo);

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, verts.size() * sizeof(float) * 8, verts.data(), GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), indices.data(), GL_STATIC_DRAW);
		
	glEnableVertexAttribArray(0); //pos
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glEnableVertexAttribArray(1); //colour
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), ((char*)0) + (sizeof(float) * 4));

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

RCCube::RCCube()
{
	std::vector<Vertex> verts =
	{
		{ XYZ1(-1, -1, -1), COLOR_GREEN }, //0
		{ XYZ1(-1, 1, 1),   COLOR_GREEN }, //1
		{ XYZ1(-1, -1, 1),  COLOR_GREEN }, //2
		{ XYZ1(-1, 1, 1),   COLOR_GREEN }, //3
		{ XYZ1(-1, -1, -1), COLOR_GREEN }, //4
		{ XYZ1(-1, 1, -1),  COLOR_GREEN }, //5

		{ XYZ1(-1, -1, -1), COLOR_GREEN }, //6
		{ XYZ1(1, -1, -1),  COLOR_GREEN }, //7
		{ XYZ1(1, 1, -1),   COLOR_GREEN }, //8
		{ XYZ1(-1, -1, -1), COLOR_GREEN }, //9
		{ XYZ1(1, 1, -1),   COLOR_GREEN }, //10
		{ XYZ1(-1, 1, -1),  COLOR_GREEN }, //11

		{ XYZ1(-1, -1, -1), COLOR_GREEN }, //12
		{ XYZ1(1, -1, 1),   COLOR_GREEN }, //13
		{ XYZ1(1, -1, -1),  COLOR_GREEN }, //14
		{ XYZ1(-1, -1, -1), COLOR_GREEN }, //15
		{ XYZ1(-1, -1, 1),  COLOR_GREEN }, //16
		{ XYZ1(1, -1, 1),   COLOR_GREEN }, //17

		{ XYZ1(-1, 1, -1),  COLOR_GREEN }, //18
		{ XYZ1(1, 1, 1),    COLOR_GREEN }, //19
		{ XYZ1(-1, 1, 1),   COLOR_GREEN }, //20
		{ XYZ1(-1, 1, -1),  COLOR_GREEN }, //21
		{ XYZ1(1, 1, -1),   COLOR_GREEN }, //22
		{ XYZ1(1, 1, 1),    COLOR_GREEN }, //23
		 
		{ XYZ1(1, 1, -1),   COLOR_GREEN }, //24
		{ XYZ1(1, -1, 1),   COLOR_GREEN }, //25
		{ XYZ1(1, 1, 1),    COLOR_GREEN }, //26
		{ XYZ1(1, -1, 1),   COLOR_GREEN }, //27
		{ XYZ1(1, 1, -1),   COLOR_GREEN }, //28
		{ XYZ1(1, -1, -1),  COLOR_GREEN }, //29

		{ XYZ1(-1, 1, 1),   COLOR_GREEN }, //30
		{ XYZ1(1, 1, 1),    COLOR_GREEN }, //31
		{ XYZ1(-1, -1, 1),  COLOR_GREEN }, //32
		{ XYZ1(-1, -1, 1),  COLOR_GREEN }, //33
		{ XYZ1(1, 1, 1),    COLOR_GREEN }, //34
		{ XYZ1(1, -1, 1),   COLOR_GREEN }, //35
	};
	
	std::vector<GLuint> indices = 
	{
		0, 1, 2,
		3, 4, 5,
		6, 7, 8,
		9, 10, 11,
		12, 13, 14,
		15, 16, 17,
		18, 19, 20,
		21, 22, 23,
		24, 25, 26,
		27, 28, 29,
		30, 31, 32,
		33, 34, 35
	};

	m_numIndices = indices.size();

	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	glGenBuffers(1, &m_vbo);
	glGenBuffers(1, &m_ibo);

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, verts.size() * sizeof(float) * 8, verts.data(), GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), indices.data(), GL_STATIC_DRAW);

	glEnableVertexAttribArray(0); //pos
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glEnableVertexAttribArray(1); //colour
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), ((char*)0) + (sizeof(float) * 4));

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

RCPlane::RCPlane(unsigned int rows, unsigned int cols)
{
	std::vector<Vertex> verts;
	//verts.resize(rows * cols); //breaks everything?
	float halfRows = rows / 2;
	float halfCols = cols / 2;
	for (unsigned int r = 0; r < rows; ++r)
	{
		for (unsigned int c = 0; c < cols; ++c)
		{
			verts.push_back({
				XYZ1((float)(c - halfCols) * 100, 2, (float)(r - halfRows) * 100),
				COLOR_BLUE
			});
			//texcoord = vec2(r / (float)rows, c / (float)cols);
		}
	}

	std::vector<GLuint> indices;
	for (unsigned int r = 0; r < (rows - 1); ++r)
	{
		for (unsigned int c = 0; c < (cols - 1); ++c)
		{
			//tri 1
			indices.push_back(r * cols + c);
			indices.push_back((r + 1) * cols + c);
			indices.push_back((r + 1) * cols + (c + 1));
			
			//tri 2
			indices.push_back(r * cols + c);
			indices.push_back((r + 1) * cols + (c + 1));
			indices.push_back(r * cols + (c + 1));
		}
	}

	m_numIndices = indices.size();

	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	glGenBuffers(1, &m_vbo);
	glGenBuffers(1, &m_ibo);

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, verts.size() * sizeof(Vertex), verts.data(), GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), indices.data(), GL_STATIC_DRAW);

	glEnableVertexAttribArray(0); //pos
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glEnableVertexAttribArray(1); //colour
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), ((char*)0) + (sizeof(float) * 4));

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
