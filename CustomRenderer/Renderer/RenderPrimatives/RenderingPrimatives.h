#pragma once
#include <glm\mat4x4.hpp>

#define XYZ1(_x_, _y_, _z_) (_x_), (_y_), (_z_), 1.f
#define RGBA(_r_, _g_, _b_, _a_) (_r_), (_g_), (_b_), (_a_)
#define UV(_u_, _v_) (_u_), (_v_)
#define COLOR_RED RGBA(1, 0, 0, 1)
#define COLOR_GREEN RGBA(0, 1, 0, 1)
#define COLOR_BLUE RGBA(0, 0, 1, 1)
#define COLOR_YELLOW RGBA(0, 1, 1, 1)

struct Vertex
{
	float x, y, z, w;
	float r, g, b, a;
};

class RenderMesh
{
public:
	unsigned int m_vao, m_vbo, m_ibo, m_numIndices;
	unsigned int m_shader;

public:
	RenderMesh() {}
};

class RCSphere : public RenderMesh
{
public:
	RCSphere(unsigned int rings, unsigned int sectors);
};

class RCCube : public RenderMesh
{
public:
	RCCube();
};

class RCPlane : public RenderMesh
{
public:
	RCPlane(unsigned int rows, unsigned int cols);
};