#pragma once

#include "..\dep\gl_core_4_4.h"

#include <GLFW\glfw3.h>
#include <assert.h>
#include <vector>
#include <map>

#include <glm\glm.hpp>
#include <glm\ext.hpp>
#include "..\Camera\FlyCamera.h"

#include "RenderPrimatives\RenderingPrimatives.h"
#include "DrawCall.h"

#define RE RenderEngine::Instance()

/*todo:
	- Base friend class RenderPass, main types: render, post-processing, UI
	- Should be able to be hot loaded between renders
	- render: Render, Shadowmap, lighting?
	- post process: cell shading, anti-aliasing
	- UI
*/
class RenderEngine
{
	bool initted = false;

	bool running;
	unsigned int m_screenW, m_screenH;

	void InitGLFW();
	void InitGL();
	void InitPrimatives();
	void InitFirstPassFrameBuffer();
	void InitPostProcessing();

	void FirstRenderPass();
	
	RenderEngine() {}
	~RenderEngine() {}

	RCSphere* primativeSphere;
	RCCube* primativeCube;
	RCPlane* primativePlane;

	std::map<std::string, GLuint> shaders;
	std::vector<DrawCall*> drawCalls;

	GLuint m_firstPassFrameBuffer, m_firstPassTexture, m_firstPassRenderBuffer;
	GLuint m_screenVAO, m_screenVBO;

	//internal shader loading
	unsigned int LoadShaderFromString(std::string name, std::string vert, std::string frag);

public:

	static RenderEngine* Instance()
	{
		static RenderEngine* instance = new RenderEngine();
		return instance;
	}

	GLFWwindow* m_window; //todo: move this to an engine class
	FlyCamera* m_camera;

	void Init(unsigned int screenW, unsigned int screenH);

	//if shader exists return it, if it doesn't add it
	unsigned int LoadShader(std::string name);

	void Update(float dt);
	void Render();

	//todo: store a pointer in each renderable object, use that pointer for this. Temporarily ref
	//draw primatives
	void AddSphere(glm::mat4& transform);
	void AddCube(glm::mat4& transform);
	void AddPlane(glm::mat4& transform);
	void AddLine(glm::vec3 min, glm::vec3 max);
};