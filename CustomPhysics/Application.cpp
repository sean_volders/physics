#include "Application.h"
#include "gl_core_4_4.h"
#include "GLFW/glfw3.h"

#include <cstdio>

#include "glm\ext.hpp"

#include "Gizmos.h"

#include <climits>

void FormatDebugOutputARB(char outStr[], size_t outStrSize, GLenum source, GLenum type,
	GLuint id, GLenum severity, const char *msg);

void __stdcall DebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity,
	GLsizei length, const GLchar *message, GLvoid *userParam);

Application::Application() {}
Application::~Application() {}

bool Application::startup()
{
	if (glfwInit() == false)
	{
		return false;
	}

#if _DEBUG
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
#endif

	window_width = 1280;
	window_height = 720;
	this->m_window = glfwCreateWindow(window_width, window_height, "Computer Graphics", nullptr, nullptr);
	if (this->m_window == nullptr)
	{
		return false;
	}

	camera = new FlyCamera();
	float fov = glm::pi<float>() * 0.25f;
	float aspect = 16 / 9.f;
	float n = 0.1f;
	float f = 1000.f;

	camera->SetSpeed(20);
	camera->SetRotationSpeed(0.1f);
	camera->SetPosition(glm::vec3(10, 10, 10));
	camera->LookAt(glm::vec3(0), glm::vec3(0, 1, 0));
	camera->SetPerspective(fov, aspect, n, f);

	glfwMakeContextCurrent(this->m_window);

	if (ogl_LoadFunctions() == ogl_LOAD_FAILED)
	{
		glfwDestroyWindow(this->m_window);
		glfwTerminate();
		return false;
	}

	int major_version = ogl_GetMajorVersion();
	int minor_version = ogl_GetMinorVersion();
	printf("Successfully loaded OpenGL version %d.%d\n",
		major_version, minor_version);


#ifdef _DEBUG
	glDebugMessageCallback((GLDEBUGPROC)DebugCallback, stderr); // print debug output to stderr
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
#endif

	Gizmos::create(3'000'000, 3'000'000);

	glfwSwapInterval(1);

	m_currTime = 0;
	m_dt = 0;
	m_prevTime = 0;

	return true;
}

void Application::shutdown()
{
	glfwDestroyWindow(this->m_window);
	glfwTerminate();
}

bool Application::update()
{
	glfwPollEvents();
	if (glfwWindowShouldClose(m_window))
	{
		return false;
	}
	if (glfwGetKey(m_window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		return false;
	}

	m_currTime = (float)glfwGetTime();
	m_dt = m_currTime - m_prevTime;
	m_prevTime = m_currTime;

	camera->Update(m_dt, m_window);
	return true;
}

void Application::draw()
{
	predraw();
}

void Application::predraw()
{
	glClearColor(0.25f, 0.25f, 0.25f, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (m_clearGizmos)
	{
		Gizmos::clear();
	}
}

void Application::postdraw()
{
	Gizmos::draw(camera->GetProjectionView());
	//Gizmos::draw(glm::ortho<float>(-100, 100, -100 / (float)(16.f / 9.f), 100 / (float)(16.f / 9.f), -1.0f, 1.0f));
	//Gizmos::draw2D(glm::ortho<float>(-100, 100, -100 / (float)(16.f / 9.f), 100 / (float)(16.f / 9.f), -1.0f, 1.0f));
	glfwSwapBuffers(m_window);
}

#if _DEBUG
void FormatDebugOutputARB(char outStr[], size_t outStrSize, GLenum source, GLenum type,
	GLuint id, GLenum severity, const char *msg)
{
	char sourceStr[512];
	const char *sourceFmt = "UNDEFINED(0x%04X)";

	switch (source)
	{
	case GL_DEBUG_SOURCE_API:             sourceFmt = "API"; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   sourceFmt = "WINDOW_SYSTEM"; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER: sourceFmt = "SHADER_COMPILER"; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:     sourceFmt = "THIRD_PARTY"; break;
	case GL_DEBUG_SOURCE_APPLICATION:     sourceFmt = "APPLICATION"; break;
	case GL_DEBUG_SOURCE_OTHER:           sourceFmt = "OTHER"; break;
	}

	_snprintf_s(sourceStr, 256, sourceFmt, source);

	char typeStr[512];
	const char *typeFmt = "UNDEFINED(0x%04X)";
	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:               typeFmt = "ERROR"; break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: typeFmt = "DEPRECATED_BEHAVIOR"; break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  typeFmt = "UNDEFINED_BEHAVIOR"; break;
	case GL_DEBUG_TYPE_PORTABILITY:         typeFmt = "PORTABILITY"; break;
	case GL_DEBUG_TYPE_PERFORMANCE:         typeFmt = "PERFORMANCE"; break;
	case GL_DEBUG_TYPE_OTHER:               typeFmt = "OTHER"; break;
	}
	_snprintf(typeStr, 256, typeFmt, type);

	char severityStr[512];
	const char *severityFmt = "UNDEFINED";
	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:   severityFmt = "HIGH";   break;
	case GL_DEBUG_SEVERITY_MEDIUM: severityFmt = "MEDIUM"; break;
	case GL_DEBUG_SEVERITY_LOW:    severityFmt = "LOW"; break;
	}

	_snprintf(severityStr, 256, severityFmt, severity);

	_snprintf(outStr, outStrSize, "OpenGL: %s [source=%s type=%s severity=%s id=%d]",
		msg, sourceStr, typeStr, severityStr, id);
}

void __stdcall DebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity,
	GLsizei length, const GLchar *message, GLvoid *userParam)
{
	(void)length;
	FILE *outFile = (FILE*)userParam;
	char finalMessage[512];
	FormatDebugOutputARB(finalMessage, 256, source, type, id, severity, message);

	if (type != GL_DEBUG_TYPE_OTHER)
	{
		fprintf(outFile, "%s\n", finalMessage);
	}
}

#endif