#pragma once
#include "glm\glm.hpp"

#ifndef NUMSHAPES
#define NUMSHAPES (JOINT+1)
#endif

enum ShapeType
{
	PLANE = 0,
	SPHERE = 1,
	BOX = 2,
	JOINT = 3,
};

class PhysicsObject
{
public:
	ShapeType _shapeID;

	virtual void update(glm::vec3 gravity, float timeStep) = 0;
	//virtual void debug() = 0;
	virtual void makeGizmo() = 0;
	virtual void resetPosition() {};
};
