#pragma once
#include "../PhysicsObject.h"
#include "RigidBody.h"

class SpringJoint : public PhysicsObject
{
	RigidBody* _connections[2];
	float dampening;
	float restLength;
	float springCoefficient;

public:
	SpringJoint(RigidBody* connection1, RigidBody* connection2,
		float springCoefficient, float dampening);

	void update(glm::vec3 gravity, float timeStep);
	//virtual void debug();
	void makeGizmo();
};