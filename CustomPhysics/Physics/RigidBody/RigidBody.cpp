#include "RigidBody.h"

void RigidBody::update(glm::vec3 gravity, float timeStep)
{
	if (!isStatic)
	{
		pos += (vel * timeStep);
		vel -= (gravity * timeStep);
		vel *= (1 - (drag * timeStep));
		rotMat = glm::rotate(rot, glm::vec3(0, 0, 1));
		rot += angularVelocity * timeStep;

		if (glm::length(vel) < MIN_LINEAR_THRESHOLD)
		{
			vel = glm::vec3(0);
		}
		if (abs(angularVelocity) < MIN_ROTATION_THRESHOLD)
		{
			angularVelocity = 0;
		}
	}
}

void RigidBody::ApplyForce(glm::vec3 force)
{
	vel += (force / mass);
}

void RigidBody::ApplyForceToActor(RigidBody * actor2, glm::vec3 force)
{
	actor2->ApplyForce(force);
	ApplyForce(-force);
}