#pragma once
#include "RigidBody.h"
class Plane : public PhysicsObject
{
public:

	glm::vec3 normal;
	float distance;
	float elasticity;

	virtual void update(glm::vec3 gravity, float timeStep) {};
	virtual void debug() {};
	virtual void makeGizmo();
	Plane(glm::vec3 normal, float distance)
		: normal(normal), distance(distance), elasticity(0.95f)
	{
		_shapeID = ShapeType::PLANE;
	}

};