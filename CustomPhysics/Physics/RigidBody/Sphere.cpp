#include "Sphere.h"
#include "Gizmos.h"

#include <climits>
void Sphere::update(glm::vec3 gravity, float timeStep)
{
	RigidBody::update(gravity, timeStep);
}

void Sphere::makeGizmo()
{
	Gizmos::addSphere(pos, radius, 10, 10, colour);
	//Gizmos::addSphere(glm::vec3(pos, 0), radius, 20, 20, colour);
}
