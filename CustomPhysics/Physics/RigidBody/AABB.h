#pragma once
#include "RigidBody.h"

class AABB : public RigidBody
{
private:
	glm::vec4 colour;

public:
	AABB(glm::vec3 position, glm::vec3 dimensions,
		glm::vec3 velocity, float mass, glm::vec4 colour)
		: RigidBody::RigidBody(position, velocity, 0, mass),
		colour(colour), dim(dimensions)
	{
		_shapeID = ShapeType::BOX;
	}

	glm::vec3 dim;

	virtual void update(glm::vec3 gravity, float timeStep);
	virtual void makeGizmo();
};