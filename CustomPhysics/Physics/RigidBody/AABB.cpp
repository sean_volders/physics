#include "AABB.h"
#include "Gizmos.h"

void AABB::update(glm::vec3 gravity, float timeStep)
{
	RigidBody::update(gravity, timeStep);
}

void AABB::makeGizmo()
{
	Gizmos::addAABBFilled(pos, dim * 0.5f, colour, &glm::mat4(1));
	//Gizmos::add2DAABBFilled(pos, dim * 0.5f, colour, &glm::mat4(1));
}