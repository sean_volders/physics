#pragma once
#include "RigidBody.h"
class Sphere : public RigidBody
{
private:

public:
	Sphere(glm::vec3 position,
		glm::vec3 velocity, float mass, float radius,
		glm::vec4 colour)
		: RigidBody::RigidBody(position, velocity, 0, mass),
		colour(colour), radius(radius)
	{
		_shapeID = ShapeType::SPHERE;
	}

	float radius;
	glm::vec4 colour;

	virtual void update(glm::vec3 gravity, float timeStep);
	virtual void makeGizmo();
	//virtual void debug();
};