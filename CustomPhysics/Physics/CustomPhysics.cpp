#include "CustomPhysics.h"
#include "RigidBody/Sphere.h"
#include "RigidBody/Plane.h"
#include "RigidBody/AABB.h"
#include "RigidBody/SpringJoint.h"

#include <GLFW\glfw3.h>

bool CustomPhysics::startup()
{
	bool ret = Application::startup();
	m_clearGizmos = true;

	m_currGunCooldown = 0;
	m_gunCooldown = 0.5f;

	physicsScene = new DIYPhysicsScene();
	physicsScene->gravity = glm::vec3(0, 10, 0);
	physicsScene->timeStep = 1;

	Sphere* ball1;
	Sphere* ball2;
	float ballRadius = 2;
	float mass = 2;
	glm::vec4 ballColour = glm::vec4(1, 1, 1, 1);
	ball1 = new Sphere(glm::vec3(0, 40, 0), glm::vec3(0), mass, ballRadius, ballColour);
	ball1->drag = 0;
	ball1->elasticity = 0.9f;
	ball1->isStatic = true;
	physicsScene->AddActor(ball1);

	int numBall = 10;
	for (int i = 1; i < numBall; ++i)
	{
		ball2 = new Sphere(glm::vec3(i*6.0f, 40, 0), glm::vec3(0), mass, ballRadius, ballColour);
		ball2->drag = 0;
		ball2->elasticity = 0.9f;
		physicsScene->AddActor(ball2);
		SpringJoint* sj = new SpringJoint(ball1, ball2, 5, .3f);
		physicsScene->AddActor(sj);
		ball1 = ball2;
	}

	ball1 = new Sphere(glm::vec3(0, 0, 0), glm::vec3(0), 90, 20, ballColour);
	ball1->isStatic = true;
	physicsScene->AddActor(ball1);

	Plane* plane = new Plane(glm::vec3(0, 1, 0), -20);
	physicsScene->AddActor(plane);
	return ret;
}

void CustomPhysics::shutdown()
{
	Application::shutdown();
}

bool CustomPhysics::update()
{
	bool ret = Application::update();
	physicsScene->timeStep = m_dt * 4;

	if (glfwGetKey(m_window, GLFW_KEY_1) == GLFW_PRESS)
	{
		for (int i = 0; i < 1; ++i)
		{
			glm::vec3 addPos(rand() % 10, rand() % 10, rand() % 10);
			glm::vec3 force((rand() & 100) - 50, (rand() & 100) - 50, 0);
			physicsScene->AddActor(new Sphere(addPos + camera->GetPosition(), force, 5, 3, glm::vec4(0, 0, 1, 0)));
		}
	}
	if (glfwGetKey(m_window, GLFW_KEY_2) == GLFW_PRESS)
	{
		for (int i = 0; i < 1; ++i)
		{
			glm::vec3 addPos(rand() % 10, rand() % 10, rand() % 10);
			glm::vec3 force((rand() & 100) - 50, (rand() & 100) - 50, 0);
			physicsScene->AddActor(new AABB(addPos + camera->GetPosition(), glm::vec3(5), force, 5, glm::vec4(0, 1, 0, 1)));
		}
	}
	if (glfwGetMouseButton(m_window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS)
	{
		if (m_currGunCooldown <= 0)
		{
			//transform
			vec3 camPos = camera->GetPosition();
			vec3 boxVel = camera->GetForward() * 20.f;
			vec3 spherePos = vec3(camPos.x, camPos.y, camPos.z);

			//geometry
			//give it velocity
			const float muzzleSpeed = 100;
			glm::vec3 direction(camera->GetForward());
			vec3 velocity = vec3(direction.x, direction.y, direction.z) * muzzleSpeed;

			physicsScene->AddActor(new Sphere(spherePos, velocity, 5, 3, glm::vec4(0, 1, 1, 1)));
			m_currGunCooldown = m_gunCooldown;
		}
	}

	m_currGunCooldown -= m_dt;

	physicsScene->Update();
	return ret;
}

void CustomPhysics::draw()
{
	Application::draw();
	physicsScene->Draw();
	Application::postdraw();
}
